CREATE DATABASE my_db;
CREATE TABLE department (
dept_id INT NOT NULL,
dept_name VARCHAR (50) NOT NULL,
PRIMARY KEY (dept_id)
);




CREATE TABLE employee  (
emp_id INT NOT NULL AUTO_INCREMENT,
first_name VARCHAR(50) NOT NULL,
surname VARCHAR(50) NOT NULL,
dob DATE NOT NULL,
date_of_joining DATE,
dept_id INT,
PRIMARY KEY ( emp_id),
FOREIGN KEY ( dept_id) REFERENCES department (dept_id)
);




INSERT INTO department (dept_id,dept_name)
VALUES (1,'ITDesk'),
(2,'Finance'),
(3,'Engineering'),
(4,'HR'),
(5,'Recruitment'),
(6,'Facility');







ALTER TABLE employee ADD annual_salary DOUBLE ;

INSERT INTO employee (first_name,surname,dob,date_of_joining,dept_id,annual_salary)
VALUES ('SAKTHI','SRI','2000-11-09','2019-10-05',1,100000),
('SRITHARAN','N','1968-02-27','1998-04-29',1,600000),
('BHARADHI','SRITHAR','1978-12-29','2005-07-07',1,500000),
('KEERTHI','SRI','2000-11-17','2019-10-05',1,100000),
('RANI','BALU','1987-07-22','2000-07-18',1,400000);

INSERT INTO employee (first_name,surname,dob,date_of_joining,dept_id,annual_salary)
VALUES ('VIGNESH','KARTHIK','1997-07-07','2019-10-05',2,100000),
('SELVI','MURUGAN','1987-07-09','1993-08-07',2,300000),
('MURUGAN','IYER','1977-07-03','1990-08-09',2,400000),
('GAYATHRI','KULANDAIVEL','2000-09-07','2020-03-06',2,100000),
('ANU','IMMANUEL','1995-05-04','2017-04-03',2,20000),

('SHANMATHI','SELVAM','1990-12-20','2000-11-22',3,200000),
('SHANMUGAM','PILLAI','1980-02-22','2000-12-28',3,200000),
('MENAGA','BALU','1992-09-06','2007-09-07',3,500000),
('KEERTHI','SURESH','1978-08-05','2000-05-08',3,400000),
('VIJAY','JOSEPH','1976-08-06','2001-08-09',3,300000),

('DHANUSH','RAGAVAN','1990-09-08','2008-05-08',4,500000),
('SELVA','RAGHAVAN','1989-06-07','2007-05-04',4,300000),
('RANJANI','THIRU','1980-09-08','2000-02-17',4,400000),
('SHIVA','KUMAR','1987-03-02','2007-02-04',4,200000),
('SELVI','RAJ','1978-03-30','1999-12-03',4,300000),

('DHIVYA','DHARSHINI','1993-02-04','2012-02-06',5,200000),
('PRIYANKA','SINGH','1998-06-03','2020-04-05',5,400000),
('DEEPTHI','ARUN','1997-04-03','2019-04-03',5,300000),
('BANU','PRIYA','1985-05-03','2005-09-02',5,400000),
('BABU','KRISHNAN','1970-05-06','2000-08-01',5,700000),

('SELVA','RANI','1978-09-07','1998-09-07',6,600000),
('BIJU','JOSEPH','1990-04-29','2010-05-01',6,400000),
('JAGAN','MUTHU','1980-03-02','2000-03-04',6,500000),
('JAANU','RAM','1990-02-04','2014-03-01',6,300000),
('PRABU','DEVA','1997-04-09','2019-05-01',6,200000);




ALTER TABLE department
MODIFY COLUMN dept_id INT NULL;




SELECT first_name
	  ,surname
	  ,dept_id
 FROM  employee
 WHERE dept_id IS NULL;
 
 
 
ALTER TABLE employee
ADD area VARCHAR(50) NOT NULL;
UPDATE employee 
SET area='chennai' WHERE emp_id=1;
UPDATE EMPLOYEE
SET area='trichy' WHERE emp_id=2;
UPDATE EMPLOYEE
SET area='madurai' WHERE emp_id=3;
UPDATE EMPLOYEE
SET area='tanjore' WHERE emp_id=4;
UPDATE EMPLOYEE
SET area='madurai' WHERE emp_id=5;
UPDATE EMPLOYEE
SET area='trichy' WHERE emp_id=6;
UPDATE EMPLOYEE
SET area='chennai' WHERE emp_id=7;
UPDATE EMPLOYEE
SET area='tanjore' WHERE emp_id=8;
UPDATE EMPLOYEE
SET area='madurai' WHERE emp_id=9;
UPDATE EMPLOYEE
SET area='tanjore' WHERE emp_id=10;
UPDATE EMPLOYEE
SET area='trichy' WHERE emp_id=11;
UPDATE EMPLOYEE
SET area='chennai' WHERE emp_id=12;
UPDATE EMPLOYEE
SET area='tanjore' WHERE emp_id=13;
UPDATE EMPLOYEE
SET area='chennai' WHERE emp_id=14;
UPDATE EMPLOYEE
SET area='madurai' WHERE emp_id=15;
UPDATE EMPLOYEE
SET area='chennai' WHERE emp_id=16;
UPDATE EMPLOYEE
SET area='trichy' WHERE emp_id=17;
UPDATE EMPLOYEE
SET area='trichy' WHERE emp_id=18;
UPDATE EMPLOYEE
SET area='tanjore' WHERE emp_id=19;
UPDATE EMPLOYEE
SET area='coimbatore' WHERE emp_id=20;
UPDATE EMPLOYEE
SET area='chennai' WHERE emp_id=21;
UPDATE EMPLOYEE
SET area='madurai' WHERE emp_id=22;
UPDATE EMPLOYEE
SET area='coimbatore' WHERE emp_id=23;
UPDATE EMPLOYEE
SET area='chennai' WHERE emp_id=24;
UPDATE EMPLOYEE
SET area='trichy' WHERE emp_id=25;
UPDATE EMPLOYEE
SET area='trichy' WHERE emp_id=26;
UPDATE EMPLOYEE
SET area='trichy' WHERE emp_id=27;
UPDATE EMPLOYEE
SET area='coimbatore' WHERE emp_id=28;
UPDATE EMPLOYEE
SET area='tanjore' WHERE emp_id=29;
UPDATE EMPLOYEE
SET area='chennai' WHERE emp_id=30;

SELECT dept_id,
       first_name,
       surname,
       area FROM employee WHERE area='trichy' AND dept_id=1;
       
UPDATE employee 
SET annual_salary = annual_salary+(annual_salary*.1) where dept_id=1;
UPDATE employee 
SET annual_salary = annual_salary+(annual_salary*.1) where dept_id=2;
UPDATE employee 
SET annual_salary = annual_salary+(annual_salary*.1) where dept_id=3;
UPDATE employee 
SET annual_salary = annual_salary+(annual_salary*.1) where dept_id=4;
UPDATE employee 
SET annual_salary = annual_salary+(annual_salary*.1) where dept_id=5;
UPDATE employee 
SET annual_salary = annual_salary+(annual_salary*.1) where dept_id=6;





SELECT AVG(annual_salary) FROM employee;





SELECT dept_id,
MAX(annual_salary) FROM employee GROUP BY dept_id;





SELECT dept_id,
MIN(annual_salary) FROM employee GROUP BY dept_id;



SELECT first_name,
       surname
        FROM employee
       WHERE emp_id=5;
SELECT dept_name FROM department
WHERE emp_id=5;




SELECT a.emp_id
      ,a.first_name
	  ,b.dept_id
      from employee a
       ,employee b
       Where a.dept_id=b.dept_id
       and b.emp_id="10";
       
       
       
SELECT e.first_name
      ,e.surname
      ,d.dept_name
      FROM my_db.employee e,my_db.department d
      Where e.dept_id = d.dept_id;
      
SELECT dob,
       first_name,
       surname
      ,sysdate()
FROM   employee 
Where  date_format(dob,'%d-%m') = date_format(sysdate(),'%d-%m');
