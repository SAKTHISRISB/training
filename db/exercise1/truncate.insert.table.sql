CREATE TABLE `database`.`department_table` (
  `department_id` INT,
  `department_name` VARCHAR(45),
  `department_location` VARCHAR(45),
  `department_head` VARCHAR(45)
  );
  
INSERT INTO `database` . `department_table`(department_name)
VALUES('PRODUCTION UNIT');

TRUNCATE `database` . `department_table`