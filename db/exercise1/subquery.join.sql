CREATE TABLE `database1`.`subject1` (
  `subject_subid` INT,
  `subject_subname` VARCHAR(10)
  );
create table database1 . student(
    id int,
    fname varchar(10),
    grade varchar(10),
    subname varchar(10),
    subid int
); 

insert into   `database1`.`subject1`(subject_subid,subject_subname) values('19','maths');   
insert into   `database1`.`subject1`(subject_subid,subject_subname) values('29','social'); 
insert into   `database1`.`subject1`(subject_subid,subject_subname) values('39','science'); 
insert into   `database1`.`subject1`(subject_subid,subject_subname) values('49','tamil'); 
insert into   `database1`.`subject1`(subject_subid,subject_subname) values('59','social'); 

select * from `database1`.`subject`;

insert into `database1`.`student`(id,fname,grade,subname,subid) values ('1','karthi','A','maths','19');
insert into `database1`.`student`(id,fname,grade,subname,subid) values ('2','selva','C','science','39');
insert into `database1`.`student`(id,fname,grade,subname,subid) values ('3','abi','B','tamil','49');
insert into `database1`.`student`(id,fname,grade,subname,subid) values ('4','reka','A','tamil','49');
insert into `database1`.`student`(id,fname,grade,subname,subid) values ('18','deepa','S','science','39');
insert into `database1`.`student`(id,fname,grade,subname,subid) values ('24','devi','A','maths','19');
insert into `database1`.`student`(id,fname,grade,subname,subid) values ('10','kavi','B','social','59');
insert into `database1`.`student`(id,fname,grade,subname,subid) values ('34','sakthi','C','social','59');
insert into `database1`.`student`(id,fname,grade,subname,subid) values ('25','eswar','C','maths','19');
insert into `database1`.`student`(id,fname,grade,subname,subid) values ('30','velu','A','tamil','49');

select * from `database1`.`student`;

select 
    id,
    fname,
    grade,
    subid,
    subname
  from `database1`.`student`
 where subid in (select subject_subid from `database1`.`subject1` where subject_subname = 'science');
 
 select 
     id,
     fname,
     grade,
     subname
   from `database1`.`student` inner join `database1`.`subject1` on subid = subject_subid;
 
 select 
     fname,
     subject_subname
  from `database1`.`subject1` left join `database1`.`student` on subject_subid = subid ;
  
 select 
     id,
     subject_subname,
     subject_subid
 from `database1`.`student` right join `database1`.`subject1` on subid = subject_subid;
 
 select 
	id,
    fname,
    grade,
    subid,
    subname,
    subject_subid,
    subject_subname
  from `database1`.`student`,`database1`.`subject1`;



