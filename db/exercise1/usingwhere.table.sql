CREATE TABLE `database`.`actor` (
  `id` INT ,
  `name` VARCHAR(45) ,
  `moviename` VARCHAR(45) ,
  `releasedyear` YEAR ,
  `totalmovie` INT
);
INSERT INTO `database`.`actor`(id,name,moviename,releasedyear,totalmovie)
VALUES ('109','surya','anjaan','2017','30');
INSERT INTO `database`.`actor`(id,name,moviename,releasedyear,totalmovie)
VALUES ('140','karthi','kaithi','2019','15');
INSERT INTO `database`.`actor`(id,name,moviename,releasedyear,totalmovie)
VALUES ('126','joe','kushi','2001','36');
INSERT INTO `database`.`actor`(id,name,moviename,releasedyear,totalmovie)
VALUES ('12','siva','remo','2018','15');

SELECT * FROM `database`.`actor` ;

SELECT 
id,
name
 FROM
 `database`.`actor` 
WHERE  
 name LIKE's%';
SELECT 
id,
name
 FROM
 `database`.`actor` 
WHERE  
 name NOT LIKE'%a';
 
INSERT INTO `database`.`actor`(id,name,moviename,releasedyear,totalmovie)
VALUES ('110','vijay','bigil','2020','50');
INSERT INTO `database`.`actor`(id,name,moviename,releasedyear,totalmovie)
VALUES ('134','sam','anjaan','2019','15');
INSERT INTO `database`.`actor`(id,name,moviename,releasedyear,totalmovie)
VALUES ('23','dhanush','3','2016','40');
INSERT INTO `database`.`actor`(id,name,moviename,releasedyear,totalmovie)
VALUES ('212','keerthi','remo','2018','20');


SELECT * FROM `database`.`actor` ;

SELECT 
id,
name,
moviename,
releasedyear
 FROM
 `database`.`actor` 
WHERE  
 releasedyear = '2018' AND moviename = 'remo';
 
SELECT 
name,
moviename,
releasedyear
 FROM
 `database`.`actor` 
WHERE  
 id='109'OR moviename = 'remo';
 
SELECT 
name,
moviename,
releasedyear
 FROM
 `database`.`actor` 
WHERE  
 moviename IN ('remo','bigil');