CREATE TABLE `database`.`student_details` (
  `reg_no` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `city` VARCHAR(45) DEFAULT 'SWEET TOWN',
  `age` INT ,
  `class` INT NOT NULL
);
INSERT INTO  `database`.`student_details`(
`reg_no`,  `name`,`age`,`class`)
VALUES (2,'esha',18,12);
INSERT INTO `database`.`student_details`(
`reg_no`,  `name` ,`city`,`age`,`class`)
VALUES (6,'fathima','CBE',12,6);
INSERT INTO  `database`.`student_details`(
`reg_no`,  `name` ,`age`,`class`)
VALUES (9,'john',15,10);


CREATE TABLE `database`.`student_details2` (
  `reg_no` INT NOT NULL UNIQUE,
  `name` VARCHAR(45) NOT NULL,
  `city` VARCHAR(45) ,
  `age` INT ,
  `class` INT 
);

INSERT INTO  `database`.`student_details2`(
`reg_no`,`name`,`age`,`class`)
VALUES (5,'esha',18,12);
INSERT INTO `database`.`student_details2`(
`reg_no`,`name` ,`city`)
VALUES (12,'fathima','CBE');
INSERT INTO  `database`.`student_details2`(
`reg_no`,`name`,`class`)
VALUES (45,'john',10);
SELECT * FROM `database`.student_details;

CREATE TABLE `database`.`student_details3` (
  `reg_no` INT NOT NULL PRIMARY KEY,
  `name` VARCHAR(45) NOT NULL,
  `city` VARCHAR(45) ,
  `class` INT 
);

INSERT INTO  `database`.`student_details3`(
`reg_no`,`name`,`class`)
VALUES (30,'esha',12);
INSERT INTO `database`.`student_details3`(
`reg_no`,`name` )
VALUES (31,'fathima');
INSERT INTO  `database`.`student_details3`(
`reg_no`,`name`,`class`)
VALUES (33,'john',10);

CREATE TABLE `database`.`student_details4` (
  `reg_no` INT NOT NULL PRIMARY KEY,
  `name` VARCHAR(45) NOT NULL,
  `city` VARCHAR(45),
 `transport` VARCHAR(45) REFERENCES `database` . `trans_details` 
);


CREATE TABLE `database` . `trans_details` (
  `transport` VARCHAR(45),
  `colour` VARCHAR(45)
);  

INSERT INTO  `database`. `trans_details`(
`transport`,`colour`)
VALUES ('cycle','pink');
INSERT INTO `database` .  `trans_details` (
`transport`,`colour`) 
VALUES ('bus','yellow');
INSERT INTO  `database`.  `trans_details`(
`transport`,`colour`)
VALUES ('cycle','green');

INSERT INTO  `database`.`student_details4`(
`reg_no`,`name`,`transport`)
VALUES (30,'esha','bus');
INSERT INTO `database`.`student_details4`(
`reg_no`,`name`,`transport`)
VALUES (31,'fathima','cycle');
INSERT INTO  `database`.`student_details4`(
`reg_no`,`name`,`transport`)
VALUES (33,'john','cycle');

CREATE TABLE `database`.`student_detailA` (
  `reg_no` INT NOT NULL UNIQUE,
  `name` VARCHAR(45) NOT NULL,
  `city` VARCHAR(45) ,
  `age` INT CHECK(age >= 10)
);

INSERT INTO  `database`.`student_details2`(
`reg_no`,`name`,`age`)
VALUES (56,'esha',18);
INSERT INTO `database`.`student_details2`(
`reg_no`,`name`,`age`)
VALUES (67,'fathima',14);
INSERT INTO  `database`.`student_details2`(
`reg_no`,`name`,`age`)
VALUES (89,'john',10);

CREATE INDEX index_name
ON `database`.`student_details2`(name);
SELECT * FROM `database`.`student_details2`