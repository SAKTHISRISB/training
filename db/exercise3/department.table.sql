CREATE TABLE education.department (
             dept_code INT NOT NULL
            ,dept_name VARCHAR(50) NULL
			,univ_code CHAR(4) NULL
			,PRIMARY KEY (dept_code)
            ,INDEX univ_code_idx (univ_code ASC) 
            ,CONSTRAINT univ_code
	  		 FOREIGN KEY (univ_code)
			 REFERENCES education.university (univ_code)
);

insert into education.department (dept_code,dept_name,univ_code)
values ('30', 'CSE', 'A');
insert into education.department (dept_code,dept_name,univ_code)
values ('70', 'IT', 'S');
insert into education.department (dept_code,dept_name,univ_code)
values ('120', 'ECE', 'Y');
insert into education.department (dept_code,dept_name,univ_code)
values ('430', 'EEE', 'B');
insert into education.department (dept_code,dept_name,univ_code)
values ('830', 'BME', 'K');