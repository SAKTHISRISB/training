CREATE TABLE education.college_department(
			 cdept_id INT NOT NULL
            ,udept_code CHAR(4) NULL
            ,college_id INT NULL
            ,PRIMARY KEY (cdept_id)
            ,INDEX college_id_idx (college_id ASC) 
            ,CONSTRAINT udept_code
             FOREIGN KEY (udept_code)
			 REFERENCES education.department (univ_code)
			 ON DELETE NO ACTION
             ON UPDATE NO ACTION,
             CONSTRAINT college_id
             FOREIGN KEY (college_id)
             REFERENCES education.college (id)
			 ON DELETE NO ACTION
             ON UPDATE NO ACTION
);

insert into education.college_department (cdept_id,udept_code,college_id)
values ('460', 'A', '500');
insert into education.college_department (cdept_id,udept_code,college_id)
values ('560', 'B', '816');
insert into education.college_department (cdept_id,udept_code,college_id)
values ('700', 'S', '618');
insert into education.college_department (cdept_id,udept_code,college_id)
values ('450', 'Y', '120');
insert into education.college_department (cdept_id,udept_code,college_id)
values ('167', 'K', '491');