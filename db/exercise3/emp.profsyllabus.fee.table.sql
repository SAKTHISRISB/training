CREATE TABLE education.employee (
			,id INT NOT NULL,
            ,name VARCHAR(100) NOT NULL,
            ,dob DATE NOT NULL,
			,email VARCHAR(50) NOT NULL,
            ,phone BIGINT NOT NULL,
            ,college_id INT NULL,
            ,cdept_id INT NULL,
            ,desig_id INT NULL,
            ,PRIMARY KEY (id),
            ,INDEX collegeid_idx (college_id ASC) VISIBLE,
            ,INDEX cdeptid_idx (cdept_id ASC) VISIBLE,
            ,INDEX desigid_idx (desig_id ASC) VISIBLE,
            ,CONSTRAINT cid
			 FOREIGN KEY (college_id)
             REFERENCES education.college (id)
			 ON DELETE NO ACTION
             ON UPDATE NO ACTION,
  CONSTRAINT cdeptid
    FOREIGN KEY (cdept_id)
    REFERENCES education.college_department (cdept_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT desigid
    FOREIGN KEY (desig_id)
    REFERENCES education.designation (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);
    
insert into  education.employee (id,name,dob,email,phone,college_id,cdept_id,desig_id)
values ('348', 'hema', '1994-04-04', 'sdc@gmail.com', '2345678', '120', '167', '150');
insert into  education.employee (id,name,dob,email,phone,college_id,cdept_id,desig_id)
values ('165', 'arun', '1990-10-10', 'cat@gmail.com', '9101112', '510', '700', '100');
insert into  education.employee (id,name,dob,email,phone,college_id,cdept_id,desig_id)
values ('920', 'mani', '1996-12-07', 'van@gmail.com', '13141517', '510', '560', '170');
insert into  education.employee (id,name,dob,email,phone,college_id,cdept_id,desig_id)
values ('725', 'radha', '1996-09-01', 'guv@gmail.com', '18171910', '220', '460', '170');
insert into  education.employee (id,name,dob,email,phone,college_id,cdept_id,desig_id)
values ('370', 'ishu', '1989-12-26', 'tcs@gmail.com', '2343409', '491', '450', '100');


CREATE TABLE `education`.`professor_syllabus` (
  `emp_id` INT NULL,
  `syllabus_id` INT NULL,
  `semester` TINYINT NOT NULL,
  INDEX `emp_id_idx` (`emp_id` ASC) VISIBLE,
  INDEX `syllabus_id_idx` (`syllabus_id` ASC) VISIBLE,
  CONSTRAINT `emp`
    FOREIGN KEY (`emp_id`)
    REFERENCES `education`.`employee` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `syllabus`
    FOREIGN KEY (`syllabus_id`)
    REFERENCES `education`.`syllabus` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
    
insert into education.professor_syllabus (emp_id,syllabus_id,semester)
values('165', '900', '3');
insert into education.professor_syllabus (emp_id,syllabus_id,semester)
values('370', '350', '4');
insert into education.professor_syllabus (emp_id,syllabus_id,semester)
values('725', '550', '5');
insert into education.professor_syllabus (emp_id,syllabus_id,semester)
values('920', '250', '2');
insert into education.professor_syllabus (emp_id,syllabus_id,semester)
values('348', '750', '1');

CREATE TABLE education.semester_fee (
             cdept_id INT NULL
			,stud_id INT NULL
            ,semester TINYINT NOT NULL
            ,amount DOUBLE NULL
            ,paid_year YEAR NULL
            ,paid_status VARCHAR(10) NOT NULL
			,INDEX stud_id_idx (stud_id ASC) 
            ,INDEX cd_id_idx (cdept_id ASC) ,
            CONSTRAINT cd_id
			FOREIGN KEY (cdept_id)
            REFERENCES education.college_department (cdept_id)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
			CONSTRAINT sd_id
            FOREIGN KEY (stud_id)
            REFERENCES education.student (id)
            ON DELETE RESTRICT
			ON UPDATE CASCADE
);
insert into education.semester_fee (cdept_id,stud_id,semester,amount,paid_year,paid_status)
values ('167', '19', '2', '80000', '2020', 'paid');
insert into education.semester_fee (cdept_id,stud_id,semester,amount,paid_year,paid_status)
values ('450', '29', '1', '100000', '2020', 'notpaid');
insert into education.semester_fee (cdept_id,stud_id,semester,amount,paid_year,paid_status)
values ('460', '49', '3', '85000', '2020', 'notpaid');
insert into education.semester_fee (cdept_id,stud_id,semester,amount,paid_year,paid_status)
values ('560', '39', '5', '50000', '2020', 'paid');
insert into education.semester_fee (cdept_id,stud_id,semester,amount,paid_year,paid_status)
values ('700', '90', '4', '65000', '2020', 'notpaid');
