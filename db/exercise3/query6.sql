INSERT INTO semester_fee (
            cdept_id
		   ,stud_id
           ,semester
		   ,amount
           ,paid_year
           ,paid_status
) 
SELECT  cdept_id
       ,id AS stud_id
       ,'4' AS semester
       ,'10000' AS amount
       ,NULL AS paid_year
       ,'notpaid' AS paid_status
  FROM student
 ORDER BY roll_number ;