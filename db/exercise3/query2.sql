SELECT student.roll_number
      ,student.name
      ,student.gender
      ,student.dob
      ,student.email
      ,student.phone
	  ,student.address
      ,(SELECT DISTINCT college.name 
          FROM college 
		 WHERE student.college_id = college.id) 
            AS college_name
	  ,(SELECT DISTINCT department.dept_name 
          FROM department 
		 WHERE department.dept_code = student.cdept_id) 
            AS department_name 
  FROM student 
 WHERE student.academic_year = '2020' 
   AND student.cdept_id 
    IN (SELECT department.dept_code 
         FROM department 
		WHERE department.dept_code 
           IN ('A','Y','K','B','S'))
   AND student.college_id 
    IN (SELECT DISTINCT university.univ_code 
         FROM university 
		WHERE university.univ_code = 'Y')
   AND student.address = 'besant nagar' 
 ORDER BY student.name