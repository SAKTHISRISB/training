CREATE TABLE education.university(
             univ_code CHAR(4)
            ,university_name VARCHAR(100)
            ,PRIMARY KEY (univ_code)
); 
insert into education.university (univ_code,university_name) values ('A', 'ANNA');
insert into education.university (univ_code,university_name) values ('B', 'BHARATHIDASAN');
insert into education.university (univ_code,university_name) values ('S', 'SASTRA');
insert into education.university (univ_code,university_name) values ('K', 'KAMAJAR');
insert into education.university (univ_code,university_name) values ('Y', 'BHARATHIYAR');

CREATE TABLE education.designation (
             id INT NOT NULL
			,name VARCHAR(30) NOT NULL
			,grade CHAR(1) NOT NULL
			,PRIMARY KEY (id)
);
  
insert into education.designation (id,name,grade) values ('100', 'HOD', 'A');
insert into education.designation (id,name,grade) values ('150', 'Staffs', 'C');
insert into education.designation (id,name,grade) values ('170', 'labassistant', 'B');
