/*
 Requirement:
  - demonstrate linked hash set to array() method in java
 Entity:
  - LinkedHashSetDemo
 Function Declaration:
  - public static void main(String args[])
 Jobs to be done:
   1.Create a class and declaring main.
   2.Inside the main creating a empty linked list and adding elements in hash set using add() method.
   3.Display the linked list element.
   4.If the linked list element is convert to array() elements.. 
   5.Display the elements.
Pseudo Code:
public class LinkedHashSetDemo {
	
	public static void main(String args[]) { 
		
		LinkedHashSet<String> set = new LinkedHashSet<String>(); 
 
		set.add("Java"); 
		set.add("is"); 
		set.add("a"); 
		set.add("Programming"); 
		set.add("Language");
 
		System.out.println("The LinkedHashSet: " + set); 
		
		String[] arr = new String[5]; 
		arr = set.toArray(arr); 

		System.out.println("The arr[] is:"); 
		for(int j = 0; j < arr.length; j++) {
			System.out.println(arr[j]); 
	    }
		
	}	
	
}      
 */


package com.training.java.core.set;

import java.util.*; 

public class LinkedHashSetDemo {
	
	public static void main(String args[]) { 
		
		LinkedHashSet<String> set = new LinkedHashSet<String>(); 
 
		set.add("Java"); 
		set.add("is"); 
		set.add("a"); 
		set.add("Programming"); 
		set.add("Language");
 
		System.out.println("The LinkedHashSet: " + set); 
		
		String[] arr = new String[5]; 
		arr = set.toArray(arr); 

		System.out.println("The arr[] is:"); 
		for(int j = 0; j < arr.length; j++) {
			System.out.println(arr[j]); 
	    }
		
	}	
	
} 