/*
 Requirement:
  - 1.java program to demonstrate adding elements, displaying, removing, and iterating in hash set
 Entity:
  - HashSetDemo
 Function Declaration:
  - public static void main(String[] args)
 Jobs to be done:
   1.Create a class and declaring main.
   2.Inside the main creating set called hash and adding elements in hash set using add() method.
   3.Check the element  
     3.1)If the element is placed then display the element.
   4.If the element is remove then print the other elements 
   5.Display all the set elements using Iterator interface
Pseudo Code:
public class HashSetDemo {		  
	   
	public static void main(String[] args) {
		 
	     HashSet<String> hash = new HashSet<String>(); 
	        
	     //Adding the elements
         hash.add("Rose"); 
	     hash.add("Lily"); 
	     hash.add("Jasmine"); 
	     hash.add("Lotus"); 
	        
	     // Displaying the HashSet 
	     System.out.println(hash); 
	     System.out.println("----------------List contains Rose or not ---------------------");
	     System.out.println(hash.contains("Rose")); 
	  
	     // Removing items from HashSet using remove() 
	     hash.remove("Lily"); 
	     System.out.println("------------------List after removing Lily------------------"); 
	     System.out.println(hash);
	     
         // Iterating over hash set items 
	     System.out.println("-----------------Iterating over list-------------------"); 
	     Iterator<String> i = hash.iterator(); 
         while(i.hasNext()) { 
             System.out.println(i.next()); 
	     } 
	}

}
   
*/


package com.training.java.core.set;

import java.util.*;

public class HashSetDemo {		  
	   
	public static void main(String[] args) {
		 
	     HashSet<String> hash = new HashSet<String>(); 
	        
	     //Adding the elements
         hash.add("Rose"); 
	     hash.add("Lily"); 
	     hash.add("Jasmine"); 
	     hash.add("Lotus"); 
	        
	     // Displaying the HashSet 
	     System.out.println(hash); 
	     System.out.println("----------------List contains Rose or not ---------------------");
	     System.out.println(hash.contains("Rose")); 
	  
	     // Removing items from HashSet using remove() 
	     hash.remove("Lily"); 
	     System.out.println("------------------List after removing Lily------------------"); 
	     System.out.println(hash);
	     
         // Iterating over hash set items 
	     System.out.println("-----------------Iterating over list-------------------"); 
	     Iterator<String> i = hash.iterator(); 
         while(i.hasNext()) { 
             System.out.println(i.next()); 
	     } 
         
	}

}
