package com.training.java.core.advancenio;

/*
9. Given a path, check if path is file or directory

WBS :

1.Requirements:
     Program to check if path is file or directory
   
2.Entities:
     FileOrDiretory
   
3.Jobs to be done:
   1.Get the file using File class passing parameter in File class.
   2.Invoke isFile method to check path is file or directory 
      2.1)Check and Print file or directory.
   
   
Pseudo Code:

public class FileOrDiretory {
	public static void main(String[] args) {
		File file = new File("C:\0dev\training\javacore -demo\advance nio\sample nio.txt");
		if (file.isFile()) {
			System.out.println("The Given path is file");
		} else {
			System.out.println("The Given path is directory");
		}
		
	}
}

*/

import java.io.File;

public class FileOrDiretory {
	public static void main(String[] args) {
		File file = new File("C:\\0dev\\training\\javacore -demo\\advance nio\\sample nio.txt");
		if (file.isFile()) {
			System.out.println("The Given path is file");
		} else {
			System.out.println("The Given path is directory");
		}
	}
}


