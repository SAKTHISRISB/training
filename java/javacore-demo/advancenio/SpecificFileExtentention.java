package com.training.java.core.advancenio;

/*
Question :
	14. Get the file names of all file with specific extension in a directory

WBS :

1.Requirements:
     Program to get the file names of all file with specific extension in a directory.
    
2.Entities:
     SpecificFileExtentention
  
3.Jobs to be done:
    1.Pass file path in File class constructor.
    2.Filter file using list method and FilenameFilter class store it in string array.
    3.Check the file name using toLowerCase method endsWith method return true or false.
    4.Print the specific file extension in the path. 
    
Pseudo Code:

public class SpecificFileExtentention {
	public static void main(String a[]) {
		File file = new File("C:\\DEV\\training1\\java\\javacore-demo\\advancenio");
		String[] list = file.list(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				if (name.toLowerCase().endsWith(".txt")) {
					return true;
				} else {
					return false;
				}
			}
		});
		for (String f : list) {
			System.out.println(f);
		}
	}
}

*/

import java.io.File;
import java.io.FilenameFilter;

public class SpecificFileExtentention {
	public static void main(String a[]) {
		File file = new File("C:\\DEV\\training1\\java\\javacore-demo\\advancenio");
		String[] ext = file.list(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				if (name.toLowerCase().endsWith(".txt")) {
					return true;
				} else {
					return false;
				}
			}
		});
		for (String exts : ext) {
			System.out.println(exts);
		}
	}
}