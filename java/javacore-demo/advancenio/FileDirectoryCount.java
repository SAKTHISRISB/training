package com.training.java.core.advancenio;

/*
Question :
	12. Number of files in a directory and number of directories in a directory

WBS :

1.Requirements:
      Program to number of files in a directory and number of directories in a directory.
    
2.Entities:
      FileDirectoryCount
    
3.Jobs to be done:
    1.Create File class and pass the file path in File class constructor argument.
    2.Invoke list method and length to find the number files in directory.
    3.Print the number files in directory.
    
Pseudo Code:

public class FileDirectoryCount {
	public static void main(String args[]) {
		File directory = new File("C:\0dev\training\javacore -demo\advance nio");
		int fileCount = directory.list().length;
		System.out.println("File Count:" + fileCount);
	}
}

*/


import java.io.File;

public class FileDirectoryCount {
	public static void main(String args[]) {
		File directory = new File("C:\\0dev\\training\\javacore -demo\\advance nio");
		int fileCount = directory.list().length;
		System.out.println("File Count:" + fileCount);
	}
}

