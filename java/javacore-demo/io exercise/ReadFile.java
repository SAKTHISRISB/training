package com.training.java.core.io;

/*
Question :
	6. Read a file using java.io.File

WBS :

1.Requirement:
     Read a file using java.io.File

2.Entity:
     ReadFile

3.Jobs to be done:
   1.Get the file
      1.1)Store the file in source String
      1.2)Check file is in given path using File class.
   2.Print the length of size using length method
   3.Print file path using getPath method.

Pseudo Code:

	public class ReaderDemo {
	
	    public static void main(String[] args) throws IOException {
	
	        String source = "C:\\0dev\\training\\javacore -demo\\io exercise\\Content.txt";
	        
	        File file = new File(source);
	
	        System.out.println(file.length());
	        
	        System.out.println(file.getPath());
	    }
	
	}

*/

import java.io.File;

public class ReadFile {

    public static void main(String[] args) {

        // source as file path
        String source = "C:\\0dev\\training\\javacore -demo\\io exercise\\Content.txt";

        // opening the file
        File file = new File(source);

        // getting the length of file
        System.out.println(file.length());

        // file or file.getPath() returns the path of the file
        System.out.println(file.getPath());
        System.out.println(file);
    }

}
