package com.training.java.core.io;

/*
Question :
	4. Write some String content using OutputStream

WBS :

1.Requirement:
      Write some String content using OutputStream

2.Entity:
      WriteOutputStream

4.Jobs to be done:
    1.Get the file using file path and store it in source String.
      1.1)Check file exist using FileOutputStream class.
    2.Write the file using OutputStream.
    3.Invoke the write of byte array to write content to file and close an outStream .
    4.OutputStream decodes the array and store as String content.

Pseudo Code:

	public class WriteOutputStream {
	    public static void main(String[] args) {
	        OutputStream outStream = new FileOutputStream(source);
	        String data = "This statement written by Output Stream";
	        byte[] byteArray = data.getBytes();
	        outStream.write(byteArray);
	        outStream.close();
	        System.out.println("File writed successfully");
	    }
	
	}

*/

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class WriteOutputStream {

    public static void main(String[] args) {
        // file path
        String source = "C:\\0dev\\training\\javacore -demo\\io exercise\\Content.txt";
        try {
            OutputStream outStream = new FileOutputStream(source);
            String text = "This Content writtern by OutputStream";

            // encodes the string to byte array
            byte[] byteArray = text.getBytes();

            // decodes and store as string
            outStream.write(byteArray);
            outStream.close();
            System.out.println("File writed successfully");
        } catch (FileNotFoundException fileNotFound) { 
            fileNotFound.getMessage();
        } catch (IOException ioe) {
            ioe.getMessage();
        }
    }

}
