/*
 Requirement:
   - To print the following pattern of the Date and Time using SimpleDateFormat.
        "yyyy-MM-dd HH:mm:ssZ"
        
 Entity:
   - PatternSimpleDateFormat
   
 Function Declaration:
   - public static void main(String[] args) throws ParseException
   
 Jobs to be done:
   1.Create a pattern with type string.
   2.Creating a SimpleDateFormat for a given pattern.
   3.Print simpleDateFormat and date.
   
Pseudo Code:
public class PatternSimpleDateFormat {

	public static void main(String[] args) throws ParseException {
		
		String pattern = "dd-MM-yyyy HH:mm:ssZ";
	    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
	    Date date = new Date();
	    
	    System.out.println(date);
	    System.out.println(simpleDateFormat.format(date));

	}

}
              
 */
package com.training.java.core.internationalization;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PatternSimpleDateFormat {

	public static void main(String[] args) throws ParseException {
		
		String pattern = "dd-MM-yyyy HH:mm:ssZ";
	    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
	    Date date = new Date();
	    
	    System.out.println(date);
	    System.out.println(simpleDateFormat.format(date));

	}

}
