/*
 Requirement:
   - Write a code to rounding the value to maximum of 3 digits by setting maximum and minimum Fraction digits.
 Entity:
   - MaxMinDigit
 Function Declaration:
   - public static void main(String[] args)
 Jobs to be done:
   1.Creating an object Locale
   2.Creating a NumberFormat for a specific Locale 
   3.Set both the minimum and maximum number of digits to use fractional part of the number.  
   4.Print numberFormat.
Pseudo Code:
public class MaxMinDigit {

	public static void main(String[] args) {
		
		Locale enLocale = new Locale("en", "US");  
		
	    NumberFormat numberFormat = NumberFormat.getInstance(enLocale);
	    
	    numberFormat.setMinimumFractionDigits(2);
	    numberFormat.setMaximumFractionDigits(3);
	    
	    System.out.println(numberFormat.format(12345.76453));

	}

}       
 */
package com.training.java.core.internationalization;

import java.text.NumberFormat;
import java.util.Locale;

public class MaxMinDigit {

	public static void main(String[] args) {
		
		Locale enLocale = new Locale("en", "US");  
		
	    NumberFormat numberFormat = NumberFormat.getInstance(enLocale);
	    
	    numberFormat.setMinimumFractionDigits(2);
	    numberFormat.setMaximumFractionDigits(3);
	    
	    System.out.println(numberFormat.format(12345.76453));

	}

}
