/*
 Requirement:
   - Write a code to change the number format to Denmark number format 
 Entity:
   - ChangeFormat
 Function Declaration:
   - public static void main(String[] args)
 Jobs to be done:
   1.Creating an object Locale
   2.Creating a NumberFormat for a specific Locale 
   3.Print Denmark fractions of a number are separated from the integer part using a comma     
   4.Print numberFormat.
Pseudo Code:
public class ChangeFormat {

	public static void main(String[] args) {
		  
	    Locale daLocale = new Locale("da", "DK");

	    NumberFormat numberFormat = NumberFormat.getInstance(daLocale);

	    System.out.println("------Denmark fractions of a number are separated from the integer part using a comma------");
	    System.out.println(numberFormat.format(100.76));
	}

}
*/
package com.training.java.core.internationalization;

import java.text.NumberFormat;
import java.util.Locale;

public class ChangeFormat {

	public static void main(String[] args) {
		  
	    Locale daLocale = new Locale("da", "DK");

	    NumberFormat numberFormat = NumberFormat.getInstance(daLocale);

	    System.out.println("------Denmark fractions of a number are separated from the integer part using a comma------");
	    System.out.println(numberFormat.format(100.76));
         
	}

}
