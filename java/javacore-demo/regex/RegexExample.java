package com.training.java.core.regex;
/*
 Requirement:
  - create a pattern for password which contains 8 to 15 characters in length
   i.Must have at least one upper case letter
  ii.Must have at least one lower case letter
 iii.Must have at least one digit
 
 Entity:
  - RegexExample
  
 Function Declaration:
  - public static void main(String[] args)
  
 Jobs to be done:
  1.Get the input from the user
  2.Declare the password of type String.
  3.Create a Pattern that
    3.1)Check the password have one upper case letter.
    3.2)Check the password have one lower case letter.
    3.3)Check the password have one digit number.
  4.Check whether the input matches the pattern
    4.1)If the input matches then print "valid password"
    4.2)If the input does not match then print "invalid password".
Pseudo Code:
public class RegexExample {
	
    public static void main(String[] args) {
		
	    String password = "NithyaSriD12345";
		Pattern p = Pattern.compile("(?=.*[0-9])(?=.*[a-z])(?=:*[A-Z]).{8,12}");
		Matcher m = p.matcher(password);
		if(m.find()) {
			System.out.println(password + " ia a valid password");
		} else {
			System.out.println(password + " is not valid password");
		}
		
    }
    
}	
    
*/
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexExample {
	
    public static void main(String[] args) {
		
	    String password = "NithyaSriD12345";
		Pattern p = Pattern.compile("(?=.*[0-9])(?=.*[a-z])(?=:*[A-Z]).{8,12}");
		Matcher m = p.matcher(password);
		if(m.find()) {
			System.out.println(password + " ia a valid password");
		} else {
			System.out.println(password + " is not valid password");
		}
		
    }
    
}	
