package com.training.java.core.regex;
/* 
1.Requirements:
   - Split any random text using any pattern you desire
2.Entities
   - SplitText
3.Function Declaration
   - public static void main (String [] args)
4.Jobs to be done
  1.Create a Class name SplitText and declare main method.
  2.Create a string and initialize a text to split.
  3.Create an array to split text to array using split() method with particular value.
  4.Print the split array of text by for loop.
Pseudo Code:
public class SplitText {  
	
	public static void main(String[] args) {  
		
		String text ="split any random text using any pattern you desire"; 
		String[] splitString = text.split("\\s", 7);
		for (int i = 0; i < splitString.length; i++){
			System.out.println(splitString[i]);
        }
		
    }
	
}
  
*/


public class SplitText {  
	
	public static void main(String[] args) {  
		
		String text ="split any random text using any pattern you desire"; 
		String[] splitString = text.split("\\s", 7);
		for (int i = 0; i < splitString.length; i++){
			System.out.println(splitString[i]);
        }
		
    }
	
}