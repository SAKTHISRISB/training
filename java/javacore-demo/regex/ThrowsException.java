package com.training.java.core.regex;
/*
1.Requirements:
   - Create a program which throws exception and fetch all the details of the exception.
2.Entity:
   - ThrowsException
3.Function Declaration:
   - public static void main(String[] args) throws FileNotFoundException, IOException
   - public static void testException(int i) throws FileNotFoundException, IOException 
4.Jobs to be done:
   1. Declare and Initialize the value in try block.
   2. Execute the try block.
   3. When an exception occurs in try block ,then go to the specific catch block.
   4. Check the condition i < 0 then throw myException ,otherwise throw the IOException.
   5. Display the output.
Jobs to be done:
public class ThrowsException {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		
		try {
			testException(-5);
			testException(-10);
		} catch(FileNotFoundException e) {
			e.printStackTrace();
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			System.out.println("Releasing resources");			
		}
		testException(15);
		
	}
	
	public static void testException(int i) throws FileNotFoundException, IOException {
		
		if(i < 0) {
			FileNotFoundException myException = new FileNotFoundException("Negative Integer "+i);
			throw myException;
		} else if(i > 10) {
			throw new IOException("Only supported for index 0 to 10");
		}

	}

}   
*/  
import java.io.FileNotFoundException;
import java.io.IOException;

public class ThrowsException {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		
		try {
			testException(-5);
			testException(-10);
		} catch(FileNotFoundException e) {
			e.printStackTrace();
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			System.out.println("Releasing resources");			
		}
		testException(15);
		
	}
	
	public static void testException(int i) throws FileNotFoundException, IOException {
		
		if(i < 0) {
			FileNotFoundException myException = new FileNotFoundException("Negative Integer "+i);
			throw myException;
		} else if(i > 10) {
			throw new IOException("Only supported for index 0 to 10");
		}

	}

}
