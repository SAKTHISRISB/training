/*
1.Requirement :
2.For the following code use the split method() and print in sentence
  String website = "https-www-google-com";
2.Entities :
SplitMethod

3.Function Declaration:
public static void main(String[] args)
	
4.Jobs to be Done :
1.Create class SplitMethod and main method.
2.Using string website and use split() method, splits the string into N substrings 
  and returns a sentence.
3.Print sentence using for each loop.

Pseudo Code:
public class SplitMethod {	
  public static void main(String[] args) {	
        String website = "https-www-google-com";
        for(String display: website.split("-")) {
              System.out.print(display+" ");
        }  	    
  }
}

*/

package com.training.java.core.regex;

public class SplitMethod {

public static void main(String[] args) {
	
	String website = "https-www-google-com";
    for(String display: website.split("-")) {
        System.out.print(display+" ");
    }  
    
}

}