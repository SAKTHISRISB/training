/*
1.Requirement :
	3) Create a user name for login website which contains
	   8-12 characters in length
	   Must have at least one upper case letter
	   Must have at least one lower case letter
	   Must have at least one digit

2.Entities :
	UserNameValidation
	
3.Function Declaration:
   	public static void main(String[] args)
	
3.Jobs to be Done :
	1.Get the input from the user
	2.Check the input is null or space ,if these conditions are not satisfied.
	3.Create a Pattern that
	  3.1)Check the name has one upper case letter.
      3.2)Check the name has one lower case letter.
      3.3)Check the name has one digit number.
	4.Check whether the input matches the pattern
	  4.1)If the input matches ,Print "valid id"
	  4.2)If the input does not matches ,Print "invalid id".
	
Pseudo Code:
public class UserNameValidation { 

    public static void main(String[] args) {
        
        String userName = "UserName04";
        Pattern p = Pattern.compile("(?=.*[0-9])(?=.*[a-z])(?=:*[A-Z]).{8,12}");
        Matcher m = p.matcher(userName);   
        if(m.find()){
            System.out.println(userName + " is valid UserName");
        } else {
            System.out.println(userName + " is not valid UserName");
        }
        
    }
    
}
	  
*/


package com.training.java.core.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserNameValidation {
	 
    public static void main(String[] args) {
        
        String userName = "UserName04";
        Pattern p = Pattern.compile("(?=.*[0-9])(?=.*[a-z])(?=:*[A-Z]).{8,12}");
        Matcher m = p.matcher(userName);   
        if(m.find()){
            System.out.println(userName + " is valid UserName");
        } else {
            System.out.println(userName + " is not valid UserName");
        }
        
    }
    
}