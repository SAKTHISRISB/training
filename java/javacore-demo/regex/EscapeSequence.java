/*
 Requirement:
    2.)write a program to display the following using escape sequence in java
       A.My favorite book is "Twilight" by Stephanie Meyer
       B.She walks in beauty, like the night, 
         Of cloudless climes and starry skies 
         And all that's best of dark and bright 
         Meet in her aspect and her eyes 
       C."Escaping characters", � 2019 Java
       
 Entity:
    - EscapeSequence  
    
 Function Declaration:
    - public static void main(String[] args)
    
 Jobs to be done:
    1.Create a string myFavouriteBook and use \" is a sequence for  displaying quotation marks on the screen
    2.Create a another string sentence using \n for create a new line 
    3.Write the copyright symbol use u00A9 sequence
    4.Display the myFavouriteBook ,sentence ,symbol.          

Pseudo Code:
public class EscapeSequence {

	public static void main(String[] args) {
		
		//create a string and use \" is a control sequence for displaying quotation marks on the screen
		String myFavoriteBook = new String ("My favorite book is \"Twilight\" by Stephanie Meyer");
	    System.out.println(myFavoriteBook);
	    
	    //create a new line use \n
	    String sentence = new String ("She walks in beauty, like the night, \nOf cloudless climes "
	    		+ " and starry skies\nAnd all that's best of dark and bright\nMeet in her aspect and her eyes...");
	    System.out.println(sentence);
	    
	    //write the well-known copyright symbol is represented by u00A9
	    String symbol = new String("\"Escaping characters\", \u00A9 2019 JAVA");
		System.out.println(symbol);
		
	}

}
 */
package com.training.java.core.regex;

public class EscapeSequence {

	public static void main(String[] args) {
		
		//create a string and use \" is a control sequence for displaying quotation marks on the screen
		String myFavoriteBook = new String ("My favorite book is \"Twilight\" by Stephanie Meyer");
	    System.out.println(myFavoriteBook);
	    
	    //create a new line use \n
	    String sentence = new String ("She walks in beauty, like the night, \nOf cloudless climes "
	    		+ " and starry skies\nAnd all that's best of dark and bright\nMeet in her aspect and her eyes...");
	    System.out.println(sentence);
	    
	    //write the well-known copyright symbol is represented by u00A9
	    String symbol = new String("\"Escaping characters\", \u00A9 2019 JAVA");
		System.out.println(symbol);
		
	}

}