/*
 - Write a program for Java String Regex Methods?
	
   Regex Method :
   
		1.matches()
		2.spilt()
		3.replaceFirst()
		4.replaceAll()

1.Requirement :
	Write a program for Java String Regex Methods.

2.Entities :
	StringRegMethod
	
3.Function Declaration:
    public static void main(String[] args)
    	
4.Job to be Done :
	1.Create class StringRegMethod and main method.
	2.Create string text and use matches() method, return true�if the regular expression matches the string,
	  and�false if not.
	3.Create one more string and use split() method, splits the string into N substrings 
	  and returns a sentence
	4.Create one more string and use replaceFirst() method, returns a new String with the first match 
	  of the regular expression passed as first parameter with the string value of the second parameter. 
	5.Create one more string and replaceAll() method, returns a new String with all matches 
	  of the regular expression passed as first parameter with the string value of the second parameter.
Pseudo Code:
public class StringRegMethod {  
    public static void main(String[] args) {
       //matches 
        String text = "one two"; 
        System.out.println("matches() method :");
        System.out.println(true); 
        
        // spilt()
        System.out.println("\nsplit() method :-");
        for(String display: Str.split("-")) {
            System.out.print(display+" ");
        }  
        System.out.println();
        
        // replaceFirst()
        System.out.println("\nreplaceFirst() method :-");
        String text1 = "one two three two one"; 
        String replaceFirst = text1.replaceFirst("two", "five"); 
        
        // replaceAll() 
        System.out.println("\nreplaceAll() method :-");
        String text2 = "one two three two one";
        String replaceAll = text2.replaceAll("two", "five");   
    }
} 
*/
package com.training.java.core.regex;

public class StringRegMethod {
     
    public static void main(String[] args) {
              
        // matches()
        String text = "one two"; 
        System.out.println("matches() method :-");
        System.out.println(text.matches(".*two"));  
        
        // spilt()
        System.out.println("\nsplit() method :-");
        String Str = "Welcome-to-FullStack-Training";
        for(String display: Str.split("-")) {
            System.out.print(display+" ");
        }  
        System.out.println();
        
        // replaceFirst()
        System.out.println("\nreplaceFirst() method :-");
        String text1 = "one two three two one"; 
        String replacefirst = text1.replaceFirst("two", "five"); 
        System.out.println(replacefirst); 
       
        // replaceAll() 
        System.out.println("\nreplaceAll() method :-");
        String text2 = "one two three two one";
        String replaceall = text2.replaceAll("two", "five");   
        System.out.println(replaceall);
         
    }

}
