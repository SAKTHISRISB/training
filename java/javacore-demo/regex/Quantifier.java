/*
1.Requirement :
	Write a program for Java String Regex quantifer.

2.Entities :
	Quantifier
	
3.Function Declaration:
    public static void main(String[] args)
	
4.Job to be Done :
	1.Create class Quantifier and main method.
	2.Create string sentence.
	3.Using pattern class and enter the quantifier pattern in compile method.
	4.Using matcher class, check whether pattern class matches the sentence.
	5.And the using while loop find matches found print the character and index.
	
Pseudo Code:
public class Quantifier {
     
    public static void main(String[] args) { 
    
        String sentence = "abaabbaaabbbaaaabb";
        Pattern p = Pattern.compile("a*");
        Matcher m = p.matcher(sentence);   
        while(m.find()){
            System.out.println(m.group()+" \tstarts at "+m.start());
        } 
        
    }
    
}	
*/

package com.training.java.core.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
 
public class Quantifier {
     
    public static void main(String[] args) { 
    
        String sentence = "abaabbaaabbbaaaabb";
        Pattern p = Pattern.compile("a*");
        Matcher m = p.matcher(sentence);   
        while(m.find()){
            System.out.println(m.group()+" \tstarts at "+m.start());
        } 
        
    }
    
}