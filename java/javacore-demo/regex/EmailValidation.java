/*
1.Requirement 
	Write a program for email-validation.

2.Entities :
	EmailValidation
	
3.Function Declaration:
   	public static void main(String[] args)
   	
4.Jobs to be Done :
	1.Get the input from the user
	2.Check the input is null or space ,if these conditions are not satisfied.
	3.Create a Pattern that
	  3.1)Check the mail-id has a small size letter
	  3.2)Check the special character is present
	4.Check whether the input matches the pattern
	  4.1)If the input matches ,Print "valid id"
	  4.2)If the input does not matches ,Print "invalid id".

Pseudo Code:
public class EmailValidation {
	 
    public static void main(String[] args) {
        
        String mail = "nithya@gmail.com";
        Pattern p = Pattern.compile("[a-z]{6}[@]");
        Matcher m = p.matcher(mail);   
        if(m.find()){
            System.out.println(mail + " is valid mail id");
        } else {
            System.out.println(mail + " is not valid mail id");
        }
        
    }
    
}
	  
*/
package com.training.java.core.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidation {
	 
    public static void main(String[] args) {
        
        String mail = "nithya@gmail.com";
        Pattern p = Pattern.compile("[a-z]{6}[@]");
        Matcher m = p.matcher(mail);   
        if(m.find()){
            System.out.println(mail + " is valid mail id");
        } else {
            System.out.println(mail + " is not valid mail id");
        }
        
    }
    
}
