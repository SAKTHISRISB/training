package com.training.java.core.regex;
/*
Requirement:
 - find the no of occurrence of a pattern in the text and also fetch the start and end index of the the occurrence
Entity:
 - OccurencePattern
Function Declaration:
 - public static void main(String[] args)
Jobs to be done:
 1.Get the input from the user
 2.Check the input is null or space or any special characters if these condition is not satisfied.
 3.Create a Pattern that
   3.1)Check whether the pattern is text.
   3.2)Fetch the start and end index
 4.Check whether the input matches the pattern.
   4.1)If the input matches ,then Print the index value. 
Pseudo Code:
public class OccurencePattern {

	public static void main(String[] args) {
		
		String sentence = "abccbbacabca";
		Pattern p = Pattern.compile("abc");
		Matcher m =p.matcher(sentence);
		while(m.find()) {
			System.out.println(m.group()+"  "+m.start());
		}

	}

}        
*/
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OccurencePattern {

	public static void main(String[] args) {
		
		String sentence = "abccbbacabca";
		Pattern p = Pattern.compile("abc");
		Matcher m =p.matcher(sentence);
		while(m.find()) {
			System.out.println(m.group()+"  "+m.start());
		}

	}

}