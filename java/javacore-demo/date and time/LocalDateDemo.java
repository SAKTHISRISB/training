/*
Requirement:
    6.To write a Java program to get the dates 10 days before and after today.
      6.1) Using Local date
      6.2) Using calendar
      
Entity:
    LocalDateDemo
    
Function Declaration:
    public static void main(String[] args)
    
Jobs to be done:
    1)Get the current date and store it in currentDate .
    2)Print the currentDate.
    3)Print the day 10 days before the currentDate.
    4)Print the day 10 days after the currentDate.
    5)An instance is created for Calendar named calendar that gets the calendar using current time zone
       and local of the system.
    6)Print the day 10 days before the currentDate.
    7)Print the day 10 days after the currentDate.
    
Pseudo code:
class LocalDateDemo {
    
    public static void main(String[] args) {

        LocalDate today = LocalDate.now();
        System.out.println("The current date is: " + " " + today);
        System.out.println("The day 10 days before the current day is: " + " " + today.plusDays(-10));
        System.out.println("The day 10 days after the current day is: " + " " + today.plusDays(10));

        Calendar calender = Calendar.getInstance();
        System.out.println("The current date is: " + " " + calender.getTime());
        
        calender.add(Calendar.DATE, -10);
        System.out.println("The day 10 days before the current day is: " + " " + calender.getTime());
        
        calender.add(Calendar.DATE, 20);
        System.out.println("The day 10 days after the current day is: " + " " + calender.getTime());
    }
}
*/
package com.training.java.core.date;

import java.time.LocalDate;
import java.util.Calendar;

public class LocalDateDemo {
    
    public static void main(String[] args) {
        
    	// using local date
        LocalDate currentDate = LocalDate.now();
        System.out.println("The current date is" + " " + currentDate);
        System.out.println("The date before 10 days from the current date : " + " " + currentDate.plusDays(-10));
        System.out.println("The date after from the current date : " + " " + currentDate.plusDays(10));
        
        // using calendar
        Calendar calender = Calendar.getInstance();
        System.out.println("The current date is: " + " " + calender.getTime());
        
        calender.add(Calendar.DATE, -10);
        System.out.println("The day 10 days before the current day is: " + " " + calender.getTime());
        
        calender.add(Calendar.DATE, 20);
        System.out.println("The day 10 days after the current day is: " + " " + calender.getTime());
        
    }
    
}