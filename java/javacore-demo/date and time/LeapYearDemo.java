/*
Requirement:
   5.1) To check whether the current or given year is leap or not.
   5.2) To find the length of the year.
    
Entity:
    LeapYearDemo.

Function Declaration:
    public static void main(String[] args)
    
Jobs to be done:
    1)Get the current year and store it in the variable currentYear of type Year.
    2)Print that currentYear.
    3) Check whether the currentYear is leap year,
           3.1) If it is a leap year, print "the current year is leap year"
           3.2) Otherwise print "the current year is not a leap year".
    4)Find the length of the currentYear and store it in lengthOfCurrentYear.
    5)print the length of the current year.
    6)Store a year in the variable givenYear.
    7)Check whether the giventYear is leap year,
           7.1) If it is a leap year, print "The given year is a leap year"
           7.2)Otherwise print "the given year is not a leap year".
    8)Find the length of the givenYear and store it in lengthOfGivenYear.
           8.1) print the length of the given year.
    
Pseudo code:
public class LeapYearDemo {
    
    public static void main(String[] args) {
        
    	Year currentYear = Year.now();
        System.out.println("The current year is " + currentYear);

        if (currentYear.isLeap() == true) {
            System.out.println("The current year is a leap year");
        } else {
            System.out.println("The current year is not a leap year");
        }
        
        int lengthOfCurrentYear = currentYear.length();
        System.out.println("The length of the current year is " + " " + lengthOfCurrentYear);
        
        Year givenYear = Year.of(2040);
        System.out.println("The given year is " + " " + givenYear);
        
        if (givenYear.isLeap() == true) {
            System.out.println("The given year is a leap year");
        } else {
            System.out.println("The given year is not a leap year");
        }
        
        int lengthOfGivenYear = currentYear.length();
        System.out.println("The length of the given year is " + " " + lengthOfGivenYear);
    }
}

*/

package com.training.java.core.date;

import java.time.Year;

public class LeapYearDemo {
    
    public static void main(String[] args) {
        
    	Year currentYear = Year.now();
        System.out.println("The current year is " + currentYear);

        if (currentYear.isLeap() == true) {
            System.out.println("The current year is a leap year");
        } else {
            System.out.println("The current year is not a leap year");
        }
        
        int lengthOfCurrentYear = currentYear.length();
        System.out.println("The length of the current year is " + " " + lengthOfCurrentYear);
        
        Year givenYear = Year.of(2040);
        System.out.println("The given year is " + " " + givenYear);
        
        if (givenYear.isLeap() == true) {
            System.out.println("The given year is a leap year");
        } else {
            System.out.println("The given year is not a leap year");
        }
        
        int lengthOfGivenYear = currentYear.length();
        System.out.println("The length of the given year is " + " " + lengthOfGivenYear);
        
    }
    
}