/*
 * Requirement:
 *      To Find out the IP Address and host name of your local computer.
 * Entities:
 * 		IpAddress
 * Function Declaration:
 * 		public static void main(String[] args)
 * JobsToBeDone:
 * 		1.Create a class named IpAddress.
 * 		2.Inside main class 
 * 			2.1.Use a method to get ipaddress and host name.
 * 		3.Print the values.
PseudoCode:
public class inetAddress{
   public static void main(String[] args){
      try{
        //IP address and host name using methods      }
      catch (UnknownHostException e){
       // print error message   }
   }
}
    */
package com.training.java.core.network;

import java.net.*;

public class IpAddress {
	
   public static void main(String[] args) {
      try {
         InetAddress my_address = InetAddress.getLocalHost();
         
		 System.out.println("LocalHost is : " + my_address);
         System.out.println("The IP address is : " + my_address.getHostAddress());
         System.out.println("The host name is : " + my_address.getHostName());
         
		 //ip address of a website
		 my_address = InetAddress.getByName("google.com.sa");
		 System.out.println("Google inetaddress is : "+ my_address);
      } catch (UnknownHostException e) {
         System.out.println( "Couldn't find the local address.");
      }
      
   }
   
}