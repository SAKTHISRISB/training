package com.training.java.core.reflection;

/*
 * Requirements:
 * 		create a Program to demonstrate @Target annotation (@Target) using interface annotation (@interface)
 *
 * Entity:
 *   	TargetAnnotation
 * 
 * Method Signature:
 *   	public static void main(String[] args)
 *   		@interface AnnotationDemo{}
 *      static @TypeAnnoDemo String method() 
 * 
 * Jobs To Be Done:
 *    	1)Create a string variable of type String and store a string value in it.
 *      2)Print the string.
 *      3)Invoke the method named method
 *    	  3.1)print a statement.
 *    
 * Pseudo Code:
 

		@interface AnnotationDemo {
		}
		
		
		public class TargetAnnotation {
		
			public static void main(String[] args) {
				@AnnotationDemo
				String string = "I am annotated with a type annotation";
				System.out.println(string);
				method();
			}
		
			static @AnnotationDemo String method() {
				System.out.println("This function's  return type is annotated");
				return null;
			}
		}

 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target(ElementType.TYPE_USE)

@interface AnnotationDemo {
	
}

public class TargetAnnotation {

	public static void main(String[] args) {
		
		@AnnotationDemo
		String string = "I am annotated with a type annotation";
		System.out.println(string);
		method();
	}

	static @AnnotationDemo String method() {
		
		System.out.println("This function's  return type is annotated");
		return null;
	}
}