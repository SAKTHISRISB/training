package com.training.java.core.reflection;
/*
Requirements:
  - Private Constructor with main class. 
Entity:
  - Private Constructor
Function Declaration:
  - public static void main(String[] args)
Jobs to be done:
  1. Create a private constructor.
  2. Create a public static method.
  3. Create an instance of Test class.
  4. Create main class.
  5. Call the instance method.
  6. Display the output.
Pseudo Code:
class Test { 
    private Test() {
        System.out.println("This is a private constructor.");
    }
    public static void instanceMethod() { 
        Test obj = new Test();
    }
}
class PrivateConstructor {
    public static void main(String[] args) {
       // call the instanceMethod()
       Test.instanceMethod();
    }   
}
*/

class Test {
 
    private Test() {
        System.out.println("This is a private constructor.");
    }

    public static void instanceMethod() { 
        Test obj = new Test();
    }

}

class PrivateConstructor {

    public static void main(String[] args) {
       // call the instanceMethod()
       Test.instanceMethod();
    }
    
}
