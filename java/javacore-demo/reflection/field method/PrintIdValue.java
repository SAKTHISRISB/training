package com.training.java.core.reflection;

/*
 * Requirement:
 * 		To write a code to set values for id and print it.
 * 
 * Entity:
 * 		PrintIdValue
 * 
 * Method Signature:
 * 		public static void main(String[] args) 
 * 
 * Jobs to be Done:
 * 		1)Assign the already defines user file to the concreteClass reference placed in the package 
 *        "com.kpr.training.reflections.User".
 * 		2)Pass the value to the User and assign the value to the id.
 * 		3)Print the value for the field id.
 * 
 * Pseudo Code:
 * 		class PrintIdValue {
 * 			public static void main(String[] args) {
 * 				Class concreteClass = Class.forName("com.kpr.training.reflections.User");
 * 				Field field = concreteClass.getField("id");
 * 
 * 				System.out.println(field.getInt(user));             
 * 				
 * 			}
 * 		}
 */

public class PrintIdValue {
	
    public static void main(String[] args) throws Exception {
    	Class<?> concreteClass = null;
		try {
			concreteClass = Class.forName("com.training.java.core.reflection.User");
		} catch(ClassNotFoundException e) {
			e.printStackTrace();
		}
    	User user = new User(1);
    	java.lang.reflect.Field field = concreteClass.getField("id");
    	System.out.println(field.getInt(user));

    	// Set value to concreteClass2
    	field.set(user, 20);

    	// Get value from concreteClass2 object
    	System.out.println(field.getInt(user));
    }
}
