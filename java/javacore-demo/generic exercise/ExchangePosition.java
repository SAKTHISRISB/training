package com.training.java.core.generic;
/*
1.Requirement:
    - Write a generic method to exchange the positions of two different elements in an array.

2.Entity:
    - ExchangePosition

3.Function Declaration:
    - public static <T> void swap(T[] list, int firstNumber, int lastNumber)
    - public static <T> void printSwap(List<T> list1, int firstNumber, int secondNumber)
    - public static void main(String[] args)
    
4.Jobs to be done:
    1.Create a class ExchangePosition
    2.Create a method swap() of type T and swap the elements using temperary variables.
    3.Create another method printSwap() and swap by using Collections.swap().
    4.Under a main method create an array and call the swap method and print the result.
    5.Create an object for List of type Integer and call printSwap() method ad print the result.
    
Pseudo Code:
public class ExchangePosition {    
    public static <T> void swap(T[] list, int firstNumber, int lastNumber) {
        T temperaryVariable = list[firstNumber];
        list[firstNumber] = list[lastNumber];
        list[lastNumber] = temperaryVariable;
        System.out.println("The swapped element is " + Arrays.toString(list) );
    }
    
    public static <T> void printSwap(List<T> list1, int firstNumber, int secondNumber) {
        Collections.<T> swap(list1, firstNumber, secondNumber);
    }
    
    public static void main(String[] args) {
        Integer[] list = {110, 250, 300, 234, 540, 600};
        swap(list, 2, 4);
        
        List<Integer> list1 = new ArrayList<>(Arrays.asList(list));
        printSwap(list1, 2, 4);
        System.out.println(list1);
        
    }
}    
*/

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ExchangePosition {
	
    public static <T> void swap(T[] list, int firstNumber, int lastNumber) {
    	
        T temperaryVariable = list[firstNumber];
        list[firstNumber] = list[lastNumber];
        list[lastNumber] = temperaryVariable;
        System.out.println("The swapped element is " + Arrays.toString(list) );
        
    }
    
    public static <T> void printSwap(List<T> list1, int firstNumber, int secondNumber) {
    	
        Collections.<T> swap(list1, firstNumber, secondNumber);
        
    }
    
    public static void main(String[] args) {
    	
        Integer[] list = {110, 250, 300, 234, 540, 600};
        swap(list, 2, 4);
        
        List<Integer> list1 = new ArrayList<>(Arrays.asList(list));
        printSwap(list1, 2, 4);
        System.out.println(list1);
        
    }
    
}

