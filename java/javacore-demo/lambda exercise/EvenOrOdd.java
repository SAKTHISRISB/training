package com.training.java.core.lambda;
/*
1.Requirements
   - Convert the following anonymous class into lambda expression.
interface CheckNumber {
    public boolean isEven(int value);
}

public class EvenOrOdd {
    public static void main(String[] args) {
        CheckNumber number = new CheckNumber() {
            public boolean isEven(int value) {
                if (value % 2 == 0) {
                    return true;
                } else return false;
            }
        };
        System.out.println(number.isEven(11));
    }
}

2.Entities
   - EvenOrOdd
   - CheckNumber (Interface)
   
3.Function Declaration
   - public boolean isEven(int value);
   - public static void main(String[] args)
   
4.Jobs to be done
   1.Create a class as EvenOrOdd with interface as CheckNumber
   2.Inside the declaring the single method with integer variable value parameters
   3.In the class main creating interface object and assigning with passing integer value of statement if condition to check 
value modulus of 2 is equal to 0 . if true return true outside if block return false.
   4.Print statement invoking the interface single method with integer value and finally return the value using assigned
interface object lambda expression statements.

Pseudo Code:
interface CheckNumber {
    public boolean isEven(int value);
}

public class EvenOrOdd {
    public static void main(String[] args) {
        CheckNumber number = (value) -> { // Convert into Java Lambda Expression 
             if (value % 2 == 0) {
                 return true;
             } else
                 return false;
        };
        System.out.println(number.isEven(12));
    }
}
*/ 	

interface CheckNumber {
	
    public boolean isEven(int value);

}

public class EvenOrOdd {
	
    public static void main(String[] args) {
    	
        CheckNumber number = (value) -> { // Convert into Java Lambda Expression 
             if (value % 2 == 0) {
                 return true;
             } else
                 return false;
        };
        System.out.println(number.isEven(12));
        
    }
    
}