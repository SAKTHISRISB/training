package com.training.java.core.list;
/*Requirement
    Contain with example.
Entity:
  Contain
Function Declaration:
   public static void main(String[] args)
Jobs to be done:
   1.Creating a string.
   2.Contains() method checks whether a particular sequence of characters is part of a given string or not.
   3.This method returns true if a specified sequence of characters is present in a given string, otherwise it returns false.
Pseudo Code:
public class Contain {
    public static void main(String[] args) {  
	    String str = "I like to do programming in python";  
	    System.out.println(str.contains("like"));
	    System.out.println(str.contains("i")); 
	    System.out.println(str.contains("Python")); 
	    System.out.println(str.contains("Like to do")); 
    }
}
   
*/

public class Contain {
	
    public static void main(String[] args) { 
    	
	    String str = "I like to do programming in python";  
	    System.out.println(str.contains("like"));
	    System.out.println(str.contains("i")); 
	    System.out.println(str.contains("Python")); 
	    System.out.println(str.contains("Like to do"));
	    
    }
    
}





