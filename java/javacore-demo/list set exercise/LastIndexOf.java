package com.training.java.core.list;
/*Requirement:
   Find the index of some value with lastindexOf()
   
Entity:
   LastIndexOf 
   
Function Declaration:
   public static void main(String[] args)
   
Jobs to be done:
 1.Creating a new array list with type Integer.
 2.Adding the values to the list using add() method.
 3.Check the values
   3.1)Print the index of last occurrence of the specified element in the list.
   3.2)Print -1 if the specified element is not present in the list.
 4.Display the values.
 
Pseudo Code:
public class LastIndexOf {
   public static void main(String[] args) {
      //ArrayList of Integer Type
      ArrayList<Integer> al = new ArrayList<Integer>();
      
      al.add(1);
      al.add(11);
      al.add(111);
      al.add(2);
      al.add(22);
      al.add(222);
      al.add(3);
      al.add(33);
      al.add(333);
      al.add(40);
      al.add(500);

      System.out.println("Last occurrence of element 500 : "+al.lastIndexOf(500));
      System.out.println("Last occurrence of element 22  : "+al.lastIndexOf(22));
      System.out.println("Last occurrence of element 3   : "+al.lastIndexOf(3));
      System.out.println("Last occurrence of element 6   : "+al.lastIndexOf(6));
   }
} 
*/

import java.util.ArrayList;

public class LastIndexOf {
	
    public static void main(String[] args) {
	   
       //ArrayList of Integer Type
       ArrayList<Integer> al = new ArrayList<Integer>();
      
       al.add(1);
       al.add(11);
       al.add(111);
       al.add(2);
       al.add(22);
       al.add(222);
       al.add(3);
       al.add(33);
       al.add(333);
       al.add(40);
       al.add(500);

       System.out.println("Last occurrence of element 500 : "+al.lastIndexOf(500));
       System.out.println("Last occurrence of element 22  : "+al.lastIndexOf(22));
       System.out.println("Last occurrence of element 3   : "+al.lastIndexOf(3));
       System.out.println("Last occurrence of element 6   : "+al.lastIndexOf(6));
      
    }
   
}