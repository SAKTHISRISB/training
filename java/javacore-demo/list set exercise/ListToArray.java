package com.training.java.core.list;
/*Requirements
    Convert the list to an array
Entity:
    ListToArray 
Function Declaration:
   public static void main(String[] args)
Jobs to be done:
   1.Create a list of type String 
   2.Add the elements in list using add() method.
   3.Converting list to array using toArray method.
   4.Print the array value in x.
Pseudo Code:
public class ListToArray { 
    public static void main(String[] args) 
    { 
        List<String> list = new LinkedList<String>(); 
        
        list.add("Keep"); 
        list.add("Learning"); 
        list.add("Something"); 
        list.add("new"); 
  
        String[] arr = list.toArray(new String[0]); 
  
        for (String x : arr) 
            System.out.print(x + " "); 
    } 
}    
*/

import java.util.*; 

public class ListToArray { 
	
    public static void main(String[] args) {
    	
        List<String> list = new LinkedList<String>(); 
        
        list.add("Keep"); 
        list.add("Learning"); 
        list.add("Something"); 
        list.add("new"); 
  
        String[] arr = list.toArray(new String[0]); 
  
        for (String x : arr) 
            System.out.print(x + " "); 
        
    } 
    
} 


