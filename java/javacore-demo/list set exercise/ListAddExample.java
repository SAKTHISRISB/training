package com.training.java.core.list;
/*Requirements:
   Create a list
      =>Add 10 values in the list.
Entity:
   ListAddExample
Function Declaration:
   public static void main(String[] args)
Jobs to be done:
 1.Create a new list of type String.
 2.Adding 10 values to the list using add() method.
 3.Display the list.
Pseudo Code:
public class ListAddExample {  
    public static void main(String[] args) {  
        ArrayList<String> list1 = new ArrayList<String>();  //Creating new List
    
        list1.add("Apple");
        list1.add("Tomato");
        list1.add("Lime");
        list1.add("Papaya");
        list1.add("Orange");
        list1.add("Banana");
        list1.add("Ant");
        list1.add("Lion");
        list1.add("Parrot");
        list1.add("Owl"); // Adding 10 values in list 
        
        //displaying elements
        System.out.println(list1);    
    }  
}

 
*/

import java.util.*;  

public class ListAddExample {  
	
    public static void main(String[] args) {  
	   
        ArrayList<String> list1 = new ArrayList<String>();  //Creating new List
      
        list1.add("Apple");
        list1.add("Tomato");
        list1.add("Lime");
        list1.add("Papaya");
        list1.add("Orange");
        list1.add("Banana");
        list1.add("Ant");
        list1.add("Lion");
        list1.add("Parrot");
        list1.add("Owl");        // Adding 10 values in list 
        
        //displaying elements
        System.out.println(list1);
        
    }  
    
}




