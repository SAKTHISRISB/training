package com.training.java.core.list;
/*
+ Create a set
    => Add 10 values
    => Perform addAll() and removeAll() to the set.
    => Iterate the set using 
        - Iterator
        - for-each
+ Explain the working of contains(), isEmpty() and give example.

1.Requirements
   - Program to print difference of two numbers using lambda expression and the single method interface 
2.Entities
   - SetDemo
3.Function Declaration
   - public static void main(String[] args) 
4.Jobs to be done
   1.Create a class and declaring main.
   2.Inside the main creating set called flowers and adding 10 flowers using add() method and printing the added set.
   3.Creating another set of newFlowers and adding 4 newFlowers using addAll() method adding flowers and newFlowers sets and after using 
removeAll() method removing added newFlowers from flowers set.
   4.Display the set using while loop with hasNext() and forEach.
   5.Checking the set with contain() method to set containing or not and to check set is empty or not using isEmpty() method.
Pseudo Code:
public class SetDemo {
	public static void main(String[] args) {
		//Creating a set and adding 10 elements to it.
		Set<String> flowers = new HashSet<>();
		
        flowers.add("ROSE");
        flowers.add("SUNFLOWER");
        flowers.add("LILY");
        flowers.add("CAMELLIA");
        flowers.add("DAISY");
        flowers.add("TULIP");
        flowers.add("ORCHID");
        flowers.add("POPPY");
        flowers.add("PANSY");
        flowers.add("MIMOSA");
        System.out.println("Creating a set and adding 10 elements to it:\n" + flowers );
        
        //Creating another set with some elements.
        Set<String> newFlowers = new HashSet<>();
        
        newFlowers.add("CHRYSANTHEMUM");
        newFlowers.add("ANEMONE");
        newFlowers.add("LAVENDER");
        newFlowers.add("HIBISCUS");
        
        //Perform addAll() method and removeAll() to the set.
        flowers.addAll(newFlowers);
        System.out.println("\t-------------addAll() method-------------------- ");
        System.out.println(flowers);
        System.out.println("\t-------------removeAll() method-------------------- ");
        flowers.removeAll(newFlowers);
        System.out.println(flowers);
        newFlowers.clear();
        
        
      //Displaying all the set elements using Iterator interface
        Iterator<String> flower = flowers.iterator();
        System.out.println("\t------------------Using While loop---------------------");
        while (flower.hasNext()) {
            System.out.println(flower.next());
        }

        //Displaying all the set elements using ForEach
        System.out.println("\t-------------Using ForEach---------------");
        flowers.forEach(System.out::println);

        //contains()
        System.out.println("\t------------------contains() method---------------------");
        System.out.println("Checks flower set contains Penstemon " + flowers.contains("PENSTEMON"));

        //isEmpty()
        System.out.println("\t------------------isEmpty() method---------------------");
        System.out.println("Check newFlowers is empty or not? " + newFlowers.isEmpty());   
	}
}
   
*/


import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetDemo {
	
	public static void main(String[] args) {
		
		//Creating a set and adding 10 elements to it.
		Set<String> flowers = new HashSet<>();
		
        flowers.add("ROSE");
        flowers.add("SUNFLOWER");
        flowers.add("LILY");
        flowers.add("CAMELLIA");
        flowers.add("DAISY");
        flowers.add("TULIP");
        flowers.add("ORCHID");
        flowers.add("POPPY");
        flowers.add("PANSY");
        flowers.add("MIMOSA");
        System.out.println("Creating a set and adding 10 elements to it:\n" + flowers );
        
        //Creating another set with some elements.
        Set<String> newFlowers = new HashSet<>();
        
        newFlowers.add("CHRYSANTHEMUM");
        newFlowers.add("ANEMONE");
        newFlowers.add("LAVENDER");
        newFlowers.add("HIBISCUS");
        
        //Perform addAll() method and removeAll() to the set.
        flowers.addAll(newFlowers);
        System.out.println("\t-------------addAll() method-------------------- ");
        System.out.println(flowers);
        System.out.println("\t-------------removeAll() method-------------------- ");
        flowers.removeAll(newFlowers);
        System.out.println(flowers);
        newFlowers.clear();
        
        
      //Displaying all the set elements using Iterator interface
        Iterator<String> flower = flowers.iterator();
        System.out.println("\t------------------Using While loop---------------------");
        while (flower.hasNext()) {
            System.out.println(flower.next());
        }

        //Displaying all the set elements using ForEach
        System.out.println("\t-------------Using ForEach---------------");
        flowers.forEach(System.out::println);

        //contains()
        System.out.println("\t------------------contains() method---------------------");
        System.out.println("Checks flower set contains Lavender " + flowers.contains("LAVENDER"));

        //isEmpty()
        System.out.println("\t------------------isEmpty() method---------------------");
        System.out.println("Check newFlowers is empty or not? " + newFlowers.isEmpty());
        
	}

}
