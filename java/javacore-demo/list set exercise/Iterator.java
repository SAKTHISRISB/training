package com.training.java.core.list;
/*Requirement
   Print the values in the list using
        -Iterator
        
Entity:
  Iterator
  
Function Declaration:
   public static void main(String[] args)
   
Jobs to be done:
   1.Create a new array list with type string.
   2.Add the elements in list using add() method.
   3.Display the list.
   4.Create an iterator for the list using iterator() method 
   5.Displaying the values after iterating through the list 
   
Pseudo Code:
public class Iterator {   
    public static void main(String[] args) { 
        // Create and populate the list 
        ArrayList<String> list = new ArrayList<>(); 
  
        list.add("Java"); 
        list.add("is"); 
        list.add("a"); 
        list.add("Programming"); 
        list.add("Language"); 
        list.add("and");
        list.add("easy"); 
        list.add("to"); 
        list.add("learn"); 
  
        // Displaying the list 
        System.out.println("The list is: \n" + list); 
  
        // Create an iterator for the list using iterator() method 
        java.util.Iterator<String> iter = list.iterator(); 
  
        // Displaying the values after iterating through the list 
        System.out.println("\nThe iterator values of list are: "); 
        while (iter.hasNext()) { 
            System.out.print(iter.next() + " "); 
        } 
    } 
}   
*/

import java.util.*;  

public class Iterator { 
	
    public static void main(String[] args) { 
    	
        // Create and populate the list 
        ArrayList<String> list = new ArrayList<>(); 
  
        list.add("Java"); 
        list.add("is"); 
        list.add("a"); 
        list.add("Programming"); 
        list.add("Language"); 
        list.add("and");
        list.add("easy"); 
        list.add("to"); 
        list.add("learn"); 
  
        // Displaying the list 
        System.out.println("The list is: \n" + list); 
  
        // Create an iterator for the list using iterator() method 
        java.util.Iterator<String> iter = list.iterator(); 
  
        // Displaying the values after iterating through the list 
        System.out.println("\nThe iterator values of list are: "); 
        while (iter.hasNext()) { 
            System.out.print(iter.next() + " "); 
        } 
        
    } 
    
}











