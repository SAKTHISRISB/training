/* Requirement: To write data to multiple files together using byte array and output stream.
 * 
 * Entity: MultipleFileWriteDemo
 * 
 * Method Signature: public static void main(String[] args);
 * 
 * Jobs to be done: 1. Create OutputStreams for files space.txt and star.txt.
 * 					2. Initialize the string value to be written to string.
 * 					3. Convert the string to byte array.
 * 					4. Write the string to file using OutputStream
 * PseudoCode:
 * 

public class MultipleFileWriteDemo {
	
	public static void main(String[] args) throws IOException {
		
		OutputStream outputStream = new FileOutputStream("C:\1dev\java\space.txt");
		OutputStream outputStream1 = new FileOutputStream("C:\1dev\java\star.txt");
		String string = "Write " + "Using " + "OutputStream"; 

		byte[] bytes = string.getBytes(); 
		outputStream.write(bytes);
		outputStream1.write(bytes);
		
		outputStream.close();
		outputStream1.close();
	}
}


 */
package com.training.java.core.iostream;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class MultipleFileWriteDemo {
	
	public static void main(String[] args) throws IOException {
		
		OutputStream outputStream = new FileOutputStream("C:\\1dev\\java\\space.txt");
		OutputStream outputStream1 = new FileOutputStream("C:\\1dev\\java\\star.txt");
		String string = "Write " + "Using " + "OutputStream"; 

		byte[] bytes = string.getBytes(); 
		outputStream.write(bytes);
		outputStream1.write(bytes);
		
		outputStream.close();
		outputStream1.close();
		
	}
	
}