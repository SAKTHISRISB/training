/*
Requirements:
   Write some String content using output stream

Entities:
   OutputStream
   
Function declaration:
   public static void main(String[] args)
   
Jobs to be done:

1.create class named as OutputStream
2.In main method,throws IOException is used
  2.1 Set the path of the file and create odject as fos
  2.2create string named as s
  2.3 Get the string in byte format and write to the file
3.Use close() method,to close the file.
4.Print the output

pseudo code:
public class OutputStream1 {

	public static void main(String[] args) throws IOException {
		 OutputStream fos = new FileOutputStream("C:\\data\\ex.text");//set the path of file
			String s = " Welcome To Java Class";
			byte[] b = s.getBytes();//converting string into byte array    
			
	                fos.write(b);
			fos.close();
			System.out.println("Success");
	 }
}	     
*/

package com.training.java.core.iostream;

import java.io.OutputStreamWriter;
import java.io.FileWriter;
import java.io.IOException;

public class OutputStream {

	public static void main(String[] args) throws IOException {
		 
		OutputStreamWriter fos = new FileWriter("C:\\1dev\\java\\space.txt");//set the path of file
		String s = " Welcome To Java Class";
		byte[] b = s.getBytes();//converting string into byte array    
			
	    fos.write(s);
		fos.close();
		System.out.println("Success");
		
	}
	
}