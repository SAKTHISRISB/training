/*Requirement: To read data from a particular file using FileReader and writes it to another, using FileWriter.
 * 
 * Entity: FileReaderWriter
 * 
 * Method Signature: public static void main(String[] args);
 * 
 * Jobs to be done: 1. Create a FileWriter to save the destination file path.
 * 					2. Create a FileReader and initialize to null for the source file path.
 * 					2. Check if exception raises in the creating file reader and writer.
 * 						2.1) Handle the exception.
 * 					3. While the file ha not reached end.
 * 						3.1) Read each characters and write it to destination file.
 * 					4. Close the FileWriter and FileReader.
PseudoCode:
public class FileReaderWriter {
	
	public static void main(String[] args) throws IOException 
    {  
        int ch; 

    	FileWriter fw = null;
        FileReader fr = null; 
        try { 
            fr = new FileReader("C:\1dev\java\space.txt"); 
            fw = new FileWriter("C:\1dev\java\star.txt"); 
            
        } catch (FileNotFoundException fe) { 
            System.out.println("File not found"); 
        } 
  
        while ((ch = fr.read()) != -1)  {
        
        	fw.write(ch); 
    	}
        fr.close(); 
        fw.close();
    } 
}
*/
package com.training.java.core.iostream;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileReaderWriter {
	
	public static void main(String[] args) throws IOException 
    {  
        int ch; 

    	FileWriter fw = null;
        FileReader fr = null; 
        try { 
            fr = new FileReader("C:\\1dev\\java\\space.txt"); 
            fw = new FileWriter("C:\\1dev\\java\\star.txt");         
        } catch (FileNotFoundException fe) { 
            System.out.println("File not found"); 
        } 
  
        while ((ch = fr.read()) != -1)  {
        
        	fw.write(ch); 
    	}
        fr.close(); 
        fw.close();
    }
	
}