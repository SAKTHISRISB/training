package com.training.java.core.exception;
//THROWS:
/*
 Requirements:
   -throws with example. 
 Entity:
   - ExampleThrows
 Function Declaration:
   - public static void main(String[] args)
 Jobs to be done:
    1. Declare and Initialize the value with type integer 
    2. Execute the try block.
    3. When an exception occurs in try block ,then go to the catch block.
    4. Check the exception, which are known to run time.
    5. Display the output.
Pseudo Code:
public class ExampleThrows {  
    int division(int firstNumber, int secondNumber) throws ArithmeticException {  
	int result = firstNumber/secondNumber;
	return result;
    }  
    public static void main(String[] args) {  
	    ExampleThrows obj = new ExampleThrows();
	    try {
	       System.out.println(obj.division(15,0));  
	    } catch(ArithmeticException e) {
	       System.out.println("You shouldn't divide number by zero");
	    }
    }  
}    
*/

public class ExampleThrows {
	
    int division(int firstNumber, int secondNumber) throws ArithmeticException { 
    	
	int result = firstNumber/secondNumber;
	return result;
	
    }  
    
    public static void main(String[] args) {
    	
	    ExampleThrows obj = new ExampleThrows();
	    try {
	       System.out.println(obj.division(15,0));  
	    } catch(ArithmeticException e) {
	       System.out.println("You shouldn't divide number by zero");
	    }
	    
    }  
    
}






