package com.training.java.core.exception;
/*
 Requirement:
   - Write a program ListOfNumbers (using try and catch block).
 Entity:
   - ListOfNumbers
   - MainList
 Function Declaration:
   - public void writeList()
   - public static void main(String[] args)
 Jobs to be done:
   1. Declared an array of integers arrayOfNumbers of size 10.
   2. A method writeList() is created.
   3. Declare and Initialize the array value.
   4. Execute the try block. 
   5. When an exception occurs in try block ,then go to the catch block.
   6. The exception is thrown to the first catch block.
   7. The first catch block does not handle an IndexOutOfBoundsException, so it is passed to the next catch block.
   8. The IndexOutOfBoundsException occurs because the array bounds for arrayOfNumbers is 0 to 9.
   9. Display the Exception.  
Pseudo Code:
class ListOfNumbers {
   public int[] arrayOfNumbers = new int[10];
   public void writeList() {
      try {
          arrayOfNumbers[10] = 11;
      } catch (NumberFormatException e1) {
          System.out.println("NumberFormatException => " + e1.getMessage());
      } catch (IndexOutOfBoundsException e2) {
          System.out.println("IndexOutOfBoundsException => " + e2.getMessage());
      }
   }
}

class MainList {
   public static void main(String[] args) {
      ListOfNumbers list = new ListOfNumbers();
      list.writeList();
   }
}
         
*/


class ListOfNumbers {
	
   public int[] arrayOfNumbers = new int[10];
   
   public void writeList() {
      try {
          arrayOfNumbers[10] = 11;
      } catch (NumberFormatException e1) {
          System.out.println("NumberFormatException => " + e1.getMessage());
      } catch (IndexOutOfBoundsException e2) {
          System.out.println("IndexOutOfBoundsException => " + e2.getMessage());
      }
   }
   
}

class MainList {
	
   public static void main(String[] args) {
	   
      ListOfNumbers list = new ListOfNumbers();
      list.writeList();
   
   }

}







