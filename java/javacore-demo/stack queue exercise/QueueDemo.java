package com.training.java.core.exercise;
/* 
1.Requirements :
  create a queue using generic type and in both implementation Priority Queue,Linked list and complete following  
  -> add at least 5 elements
  -> remove the front element
  -> search a element in stack using contains key word and print boolean value value
  -> print the size of queue
  -> print the elements using Stream 
  
2.Entities:
  - QueueDemo 
  
3.Function Declaration:
  - public void linkedList()
  - public void priority()
  - public static void main(String[] args)
  
4.Job to be done :  
   1.Create a class name QueueDemo and declare main method
   2.In the main creating object for class and invoking the two methods.
   3.In the linkedList() method creating Queue with generic String type with initialize LinkedList 
and creating stream for displaying the Stack elements.
   4.Adding 5 values to Queue using add() method and using remove() method removing value specified value.
   5.Search the specified value using search() method and find size of stack using size() method.
   6.Using contains() method finding the given value is in the Queue or not and using size() method finding size of queue.
   7.Printing the queue values using stream and for each value.
   8.In the priority() method creating queue with initialize PriorityQueue and also creating stream to print the queue values.
   9.Adding the values to queue using add() method and finding specified value using contains() method.
   10.Finding the size using size() method and printing the PriorityQueue values using stream and for each.
   11.In main method create a instance queueDemo.
   12.Print linkedList and priorityQueue.
   
Pseudo Code:
public class QueueDemo {
	public void linkedList() {
		Queue<String> flowers = new LinkedList<String>();
	    Stream<String> stream = flowers.stream(); 
	    flowers.add("ROSE");
	    flowers.add("JASMINE");
	    flowers.add("LAVENDER");
	    flowers.add("LILY");
	    flowers.add("SUNFLOWER");
	    flowers.remove();     
	    System.out.println(flowers.contains("LAVENDER"));
	    System.out.println(flowers.size());       
	    stream.forEach((element) -> {
	        System.out.println(element);  
	    }); 
	}
	public void priority() {
		Queue<String> flowers = new PriorityQueue<String>();
	    Stream<String> stream = flowers.stream();
	    flowers.add("ROSE");        
	    flowers.add("JASMINE");
	    flowers.add("LAVENDER");
	    flowers.add("LILY");
	    flowers.add("SUNFLOWER");
	    flowers.remove();
	    System.out.println(flowers.contains("SUNFLOWER"));
	    System.out.println(flowers.size());   
	    stream.forEach((element) -> {
            System.out.println(element); 
	    });    //Process Stack Using Stream
    }
    public static void main(String [] args) {
    	QueueDemo queue = new QueueDemo();	
    	    System.out.println("-----------------Linked List--------------------");
    	    queue.linkedList();
    	    System.out.println("-----------------Priority---------------------");
    	    queue.priority();	    
    }
}   
*/
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.stream.Stream;

public class QueueDemo {
	
	public void linkedList() {
		
		Queue<String> flowers = new LinkedList<String>();
	    Stream<String> stream = flowers.stream();
	    
	    flowers.add("ROSE");
	    flowers.add("JASMINE");
	    flowers.add("LAVENDER");
	    flowers.add("LILY");
	    flowers.add("SUNFLOWER");
	    flowers.remove();
	        
	    System.out.println(flowers.contains("LAVENDER"));
	    System.out.println(flowers.size());       
	    stream.forEach((element) -> {
	        System.out.println(element);  
	    }); 
	}
	
	public void priority() {
		
		Queue<String> flowers = new PriorityQueue<String>();
	    Stream<String> stream = flowers.stream();
	    
	    flowers.add("ROSE");        
	    flowers.add("JASMINE");
	    flowers.add("LAVENDER");
	    flowers.add("LILY");
	    flowers.add("SUNFLOWER");
	    flowers.remove();
	    
	    System.out.println(flowers.contains("SUNFLOWER"));
	    System.out.println(flowers.size());   
	    stream.forEach((element) -> {
            System.out.println(element); 
	    });    //Process Stack Using Stream
    }
	
    public static void main(String [] args) {
    	
    	QueueDemo queue = new QueueDemo();
    	
    	    System.out.println("-----------------Linked List--------------------");
    	    queue.linkedList();
    	    System.out.println("-----------------Priority---------------------");
    	    queue.priority();
    	    
    }

}