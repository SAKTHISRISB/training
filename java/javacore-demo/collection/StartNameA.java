package com.training.java.core.collection;
/*
Requirement:
 - LIST CONTAINS 10 STUDENT NAMES krishnan, abishek, arun,vignesh, kiruthiga, murugan,adhithya,balaji,vicky, priya 
   and display only names starting with 'A'.
Entity:
 - StartNameA
Function Declaration:
 - public static void main(String[] args)    
Jobs to be done:
 1.Create a list and add 10 student names in the list
 2.If given the student name
   2.1)Check the name start with "a"
 3.Print the names.
Pseudo Code:
public class StartNameA {
	
   public static void main(String[] args) {
		
   	List<String> list = new LinkedList<String>(); 
   	
   	Scanner input = new Scanner(System.in);
   	list.add("Krishnan");
   	list.add("Abishek");
   	list.add("Arun");
   	list.add("Vignesh");
   	list.add("Kiruthiga");
   	list.add("Murugan");
   	list.add("Adhithya");
   	list.add("Balaji");
   	list.add("Vicky");
   	list.add("Priya");
   	
   	String[] arr = list.toArray(new String[0]); 
   	System.out.println("Students Name List: \n" + list);  
   	System.out.println("Student Name Start With A : ");
       for(String x : arr) {
       	if(x.startsWith("A")) {
              System.out.println(x); 
           }       	
       } 
       
   }
   
}	  
*/

import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;

public class StartNameA {
	
   public static void main(String[] args) {
		
   	List<String> list = new LinkedList<String>(); 
   	
   	Scanner input = new Scanner(System.in);
   	list.add("Krishnan");
   	list.add("Abishek");
   	list.add("Arun");
   	list.add("Vignesh");
   	list.add("Kiruthiga");
   	list.add("Murugan");
   	list.add("Adhithya");
   	list.add("Balaji");
   	list.add("Vicky");
   	list.add("Priya");
   	
   	String[] arr = list.toArray(new String[0]); 
   	System.out.println("Students Name List: \n" + list);  
   	System.out.println("Student Name Start With A : ");
       for(String x : arr) {
       	if(x.startsWith("A")) {
              System.out.println(x); 
           }       	
       } 
       
   }
   
}	



