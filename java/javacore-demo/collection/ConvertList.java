package com.training.java.core.collection;
/*
 Requirement:
  - create an array list with 7 elements, and create an empty linked list add all elements of the array list to linked list ,
	traverse the elements and display the result
 Entity:
  - ConvertList
 Function Declaration:
  - public static <T> List<T> convertALtoLL(List<T> aL)
  - public static void main(String args[])
 Jobs to be done:
  1.Create a class ConvertList and get the ArrayList to be converted into LinkedList.
  2.Create an empty LinkedList
  3.Iterate through the items in the ArrayList.
  4.For each item, add it to the LinkedList
  5.Return the LinkedList elements.
Pseudo Code:
public class ConvertList { 
    // Generic function to convert an ArrayList to LinkedList 
	public static <T> List<T> convertALtoLL(List<T> aL) { 
		
		List<T> list = new LinkedList<>();  // Create an empty LinkedList 

		for (T t : aL) { 
			list.add(t); 
		} 
		
		return list; 
	} 

	public static void main(String args[]) { 
		
		List<String> aL = Arrays.asList("Java", "is", "a","programming","language","and","easy","to","understand"); 
		System.out.println("ArrayList: " + aL); 

		List<String> list = convertALtoLL(aL); 
		System.out.println("LinkedList : " + list); 
	}
	
}    	
 */

import java.util.*; 
import java.util.stream.*; 

public class ConvertList { 
    // Generic function to convert an ArrayList to LinkedList 
	public static <T> List<T> convertALtoLL(List<T> aL) { 
		
		List<T> list = new LinkedList<>();  // Create an empty LinkedList 

		for (T t : aL) { 
			list.add(t); 
		} 
		
		return list; 
	} 

	public static void main(String args[]) { 
		
		List<String> aL = Arrays.asList("Java", "is", "a","programming","language","and","easy","to","understand"); 
		System.out.println("ArrayList: " + aL); 

		List<String> list = convertALtoLL(aL); 
		System.out.println("LinkedList : " + list); 
		
	}
	
} 

