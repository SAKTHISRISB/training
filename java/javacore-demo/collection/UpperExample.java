package com.training.java.core.collection;
/*
 Requirement:
  - 8 districts are shown below Madurai, Coimbatore, Theni, Chennai, Karur, Salem, Erode, Trichy 
    to be converted to UPPERCASE.
 Entity:
  - UpperExample
 Function Declaration:
  - public static void main(String[] args)
 Jobs to be done:
  1.Get the input from the user
  2.Declare and Initialize the value of type String
  3.Creating a list and adding elements in list.
  4.Convert the string values to UpperCase.
  5.Print the value. 
Pseudo Code:
public class UpperExample {
	
    public static void main(String[] args) {
	   
        List<String> list = Arrays.asList("Madurai", "Coimbatore", "Theni", "Chennai", "Karur", "Salem", "Erode", "Trichy");
      
        System.out.println("List = " + list);
        System.out.println("\nUppercase strings = ");
        list.stream().map(district -> district.toUpperCase()) .forEach(district -> System.out.print(district + " "));
        
    }

}      
 */
import java.util.Arrays;
import java.util.List;

public class UpperExample {
	
    public static void main(String[] args) {
	   
        List<String> list = Arrays.asList("Madurai", "Coimbatore", "Theni", "Chennai", "Karur", "Salem", "Erode", "Trichy");
      
        System.out.println("List = " + list);
        System.out.println("\nUppercase strings = ");
        list.stream().map(district -> district.toUpperCase()) .forEach(district -> System.out.print(district + " "));
        
    }

}
