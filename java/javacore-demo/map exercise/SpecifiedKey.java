package com.training.java.core.map;

/*
1.Requirements
   - Java program to copy all of the mappings from the specified map to another map.
   
2.Entities
   - SpecifiedKey
   
3.Function Declaration
   - public static void main(String[] args)
   
4.Jobs to be done
   1.Create a class as SpecifiedKey and declaring the main.
   2.In the class main creating object for HashMap with integer and String called flowers.
   3.Adding five values using put() method and creating another hashMap flowers2 for copying from flowers 
to flowers2 using putAll() method.
   4.Print the copied hashMap flowers2.

Pseudo Code:
public class SpecifiedKey {

    public static void main(String[] args) {   
    
	    //Creating hashMap 
        HashMap<Integer, String> flowers = new HashMap<Integer, String>();

        //Adding elements to HashMap
        flowers.put(4, "ROSE");
        flowers.put(2, "JASMINE");
        flowers.put(3, "SUNFLOWER");
        flowers.put(5, "LILY");
        flowers.put(1, "HIBISCUS");
      
        HashMap<Integer, String> flowers2 = new HashMap<Integer, String>();
      
        //Copy all of the mappings from the specified map to another map
        flowers2.putAll(flowers);
        System.out.println(flowers2);
        
    }

}   
*/


import java.util.HashMap;

public class SpecifiedKey {
	
    public static void main(String[] args) {
    	
	    //Creating hashMap 
        HashMap<Integer, String> flowers = new HashMap<Integer, String>();

        /*Adding elements to HashMap*/
        flowers.put(4, "ROSE");
        flowers.put(2, "JASMINE");
        flowers.put(3, "SUNFLOWER");
        flowers.put(5, "LILY");
        flowers.put(1, "HIBISCUS");
      
        HashMap<Integer, String> flowers2 = new HashMap<Integer, String>();
      
        //Copy all of the mappings from the specified map to another map
        flowers2.putAll(flowers);
        System.out.println(flowers2);
        
    }

}


