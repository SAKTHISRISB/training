package com.training.java.core.map;
/*
1.Requirements
   - Java program to get the portion of a map whose keys range from a given key to another key.
   
2.Entities
   - GetPortion
   
3.Function Declaration
   - public static void main(String[] args)
   
4.Jobs to be done
   1.Create a class as GetPortion and declaring the main.
   2.In the class main creating object for TreeMap with integer and String called flowers.
   3.Add the key value elements in put() method.
   4.Selecting the portion of the TreeMap using subMap() method.
   5.Print the Sub map key values.
   
Pseudo Code:
public class GetPortion {
	
	public static void main(String[] args) {	
		
		 //Creating TreeMap and SortedMap
	    TreeMap<Integer, String> flowers = new TreeMap<Integer, String>();
	      
	    //Adding elements to HashMap
	    flowers.put(34, "ROSE");
	    flowers.put(20, "LILY");
	    flowers.put(13, "SUNFLOWER");
	    flowers.put(5, "JASMINE");
	    flowers.put(1, "HIBISCUS");	 
         
    	System.out.println("-------------Sub map key-values------------------- \n" + flowers.subMap(10, 40));
    	   
	}
	
}
   
*/


import java.util.TreeMap; 

public class GetPortion {
	
	public static void main(String[] args) {	
		
		 //Creating TreeMap and SortedMap
	    TreeMap<Integer, String> flowers = new TreeMap<Integer, String>();
	      
	    /*Adding elements to HashMap*/
	    flowers.put(34, "ROSE");
	    flowers.put(20, "LILY");
	    flowers.put(13, "SUNFLOWER");
	    flowers.put(5, "JASMINE");
	    flowers.put(1, "HIBISCUS");	 
         
    	System.out.println("-------------Sub map key-values------------------- \n" + flowers.subMap(10, 40));
    	   
	}
	
}
