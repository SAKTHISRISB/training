1)Concurrency:
     i)Concurrency means that an application is making progress on more than one task at the same time.
	ii)If the computer has one CPU the application may not make progress on more than one task at exactly the same time,
	   but more than one task is being processed at a time inside the application.
   iii)It does not completely finish one task before it begins the next.
    iv)Instead, the CPU switches between the different tasks until the tasks are complete.
     v)An application may process one task at at time (sequentially) or work on multiple tasks at the same time (concurrently).

Parallelism:
    i) Parallelism means that an application splits its tasks up into smaller subtasks which can be processed in parallel,
       for instance on multiple CPUs at the exact same time.
    ii) An application may process the task serially from start to end, or split the task up into subtasks which can be completed in parallel.
	
	
	