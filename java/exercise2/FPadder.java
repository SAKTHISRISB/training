/*
    Question : Create a program that is similar to the previous one but instead of reading integer arguments, it reads floating-point arguments.
               It displays the sum of the arguments, using exactly two digits to the right of the decimal point.
               For example, suppose that you enter the following: java FPAdder 1 1e2 3.0 4.754
               The program would display 108.75. Depending on your locale, the decimal point might be a comma (,) instead of a period (.).
*/

/*
    Requirements : To create a program to read floating point arguments.
                   To display the sum of arguments using exactly two digits to the right of the decimal point.
*/
				 
/*
    Entities : public class FPAdderDemo
*/

/*  Method Signature : public static void main(String[] args)
 */

/*  Jobs to be done : 1.Length of the arguments is checked whether it is less than 2.
                      2.If the length is less than 2 ,the message " This program requires two command-line arguments." is displayed.
                      3.If the length is not less than 2 variable sum is initialized with value 0.0.
                      4.for loop is created with i=0,i<numArgs,i++.
                      5.Again sum value is changed by sum + Double.valueOf(args[i]).doubleValue().
                      6.An object myFormatter is created for DecimalFormat.
                      7.Variable output is initialized as myFormatter.format(sum).
                      8.Value in variable output is displayed.					 
 */

//Answer:


import java.text.DecimalFormat;
public class FPAdder {
    public static void main(String[] args) {
	int numArgs = args.length;
        if (numArgs < 2) {
            System.out.println("This program requires two command-line arguments.");
        } else {
	    double sum = 0.0;
	    for (int i = 0; i < numArgs; i++) {
                sum += Double.valueOf(args[i]).doubleValue();
	    }
	    DecimalFormat myFormatter = new DecimalFormat("###,###.##");
	    String output = myFormatter.format(sum);
            System.out.println(output);
        }
    }
}