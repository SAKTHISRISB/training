/*  
    Question :
	
	What's wrong with the following program? And fix it.

    public class SomethingIsWrong {
        public static void main(String[] args) {
            Rectangle myRect;
            myRect.width = 40;
            myRect.height = 50;
            System.out.println("myRect's area is " + myRect.area());
        }
    }
*/

/*  
    Requirements : To debug the given program.
*/

/*  
    Entities : public class SomethingIsWrong 
*/

/*  
    Method Signature : public static void main(String[] args)
*/

/*  
    Jobs to be done : 1. myRect object is given value
*/

	
//Ans :

    //myrect object is declared as null.So it will give nullpointerexception.




//fixed program :


        public class SomethingIsRight {
                  public static void main(String[] args) {
                      Rectangle myRect = new Rectangle();
                      myRect.width = 40;
                      myRect.height = 50;
                      System.out.println("myRect's area is " + myRect.area());
                   }
                }
