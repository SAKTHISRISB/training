/*
    Question :
	
    Given the following class, called NumberHolder,
    write some code that creates an instance of the class and initializes its two member variables with provided values,
    and then displays the value of each member variable.
	
	public class NumberHolder {
        public int anInt;
        public float aFloat;
    }
*/

/*  
    Requirements : To create an instance for the given class.
	               To initialize two member variables of the class with given values.
				   To display the value of each member variable.
*/

/*  
    Entities : public class NumberHolder 
*/

/* 
    Method Signature : public static void main(String[] args)
*/

/*
    Jobs to be done : 1. Declare two variables anInt and aFloat.
                      2. Inside the main function an object is created.
                      3. It is named NumberHolder1 for the class NumberHolder.
					  3. Values are initialized to variables anInt and aFloat using object NumberHolder1.
					  4. The values of anInt and aFloat is displayed.
*/
    
//Ans:
 
 public class DisplayNumberHolder{
    public static void main(String[] args) {
	NumberHolder NumberHolder1 = new NumberHolder();
	NumberHolder1.anInt = 10;
	NumberHolder1.aFloat = 5.5f;
	System.out.println(NumberHolder1.anInt);
	System.out.println(NumberHolder1.aFloat);
    }
}