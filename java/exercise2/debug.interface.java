/*  
    Question :
	
	What is wrong with the following interface? and fix it.
	
    public interface SomethingIsWrong {
        void aMethod(int aValue){
            System.out.println("Hi Mom");
        }
    }
*/

/*  
    Requirements : To debug the given interface program.
*/

/*  
    Entities : public interface SomethingIsWrong 
*/

/*  
    Method Signature : default void aMethod(int aValue)
*/

/*  
    Jobs to be done : 1. The access specifier is given to the method as "default".
*/

	
//Ans :

    //Interface class does not contain any method implementation in it.	
	
	
public interface SomethingIsWrong {
    default void aMethod(int aValue) {
        System.out.println("Hi Mom");
    }
}