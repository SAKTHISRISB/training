/*
    Write a program that computes your initials from your full name and displays them.
*/

/*
    Requirements : To create and display a program which computes the initials from full name.
                   
*/

/*
    Entities : public class ComputeInitials
*/

/*
    Method Signature : public static void main(String[] args)
*/

/*  
    Jobs to be done : 1. The variable myName is initialized with value "Fred F. Flintstone".
                      2. The object myInitials is created for class StringBuffer.
                      3. The variable length is initialized with myName.length().
                      4. For loop is created with i = 0,i < length,i++.
                      5. Inside the if statement Character is checked whether it is uppercase.
                      6. If it is true Initial is appended by using function myInitials.append(myName.charAt(i)))
                      7. The initial is displayed.
*/

Answer : 
public class ComputeInitials {
    public static void main(String[] args) {
        String myName = "Fred F. Flintstone";
        StringBuffer myInitials = new StringBuffer();
        int length = myName.length();

        for (int i = 0; i < length; i++) {
            if (Character.isUpperCase(myName.charAt(i))) {
                myInitials.append(myName.charAt(i));
            }
        }
        System.out.println("My initials are: " + myInitials);
    }
}